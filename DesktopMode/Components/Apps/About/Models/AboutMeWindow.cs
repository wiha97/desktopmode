using DesktopMode.Models;

namespace DesktopMode.Components.Apps.About.Models;


public class AboutMeWindow : DesktopWindow
{
    public AboutMeWindow()
    {
        Name = "about";
        Width = 570;
        Height = 270;
        Type = WinType.About;
        Icon = $"assets/aboutme.png";
    }
}