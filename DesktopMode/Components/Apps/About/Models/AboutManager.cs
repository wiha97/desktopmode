namespace DesktopMode.Components.Apps.About.Models;

public class AboutManager
{
    public AboutModel About { get; set; }

    public AboutManager()
    {
        About = new AboutModel()
        {
            Name = "Wilhelm Hansson",
            NameComment = "Call me Wille",
            Age = 27,
            AgeComment = "TODO: Change to 27 on 11/03/2025",
        };
    }
}