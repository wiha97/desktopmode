namespace DesktopMode.Components.Apps.About.Models;

public class AboutModel
{
    public string Name { get; set; }
    public string? NameComment { get; set; }
    public int Age { get; set; }
    public string? AgeComment { get; set; }
    public Contact? ContactInfo { get; set; }
    public List<Interest>? Interests { get; set; }
    public List<Skill>? Skills { get; set; }
}

public class Contact
{
    public string Tel { get; set; }
    public string Email { get; set; }
    public string Git { get; set; }
}

public class Interest
{
    public string Name { get; set; }
    public string Description { get; set; }
}

public class Skill
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string Project { get; set; }
}