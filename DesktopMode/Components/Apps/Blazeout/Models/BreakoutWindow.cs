using DesktopMode.Models;

namespace DesktopMode.Components.Apps.Blazeout.Models;

public class BreakoutWindow : DesktopWindow
{
    public BreakoutWindow()
    {
        Name = "breakout";
        Width = 550;
        Height = 740;
        Type = WinType.App;
        Icon = $"assets/breakout.png";
        SetParam();
    }
}