﻿namespace DesktopMode.Components.Apps.Blazeout.Models
{
    public class Projectile
    {
        public double Speed { get; set; } = 3;    //  Absolute velocity of projectile
        public double XVelocity { get; set; }       //  Velocity of X-axis
        public double YVelocity { get; set; }       //  Velocity of Y-axis
        public double Angle { get; set; }           //  Calculated angle grabbed from bar's velocity
        public double Direction { get; set; }
        public double YPos { get; set; }
        public double XPos { get; set; }
        public int Size { get; set; } = 10;
        public string Position { get; set; }        // CSS to visualize location

        public void SideMovement(double vel)
        {
            XVelocity = XPos / vel;
            // YVelocity = YPos / Speed;

            if (XVelocity < XPos)
            {
                
            }
        }
    }
}
