﻿using System.ComponentModel;
using DesktopMode.Models;
using Microsoft.AspNetCore.Components.Web;

namespace DesktopMode.Components.Apps.Blazeout.Models
{
    public class BreakoutManager
    {
        DesktopWindow win;
        public int PlayWidth { get; set; } = 375;
        public int PlayHeight { get; set; }
        public Bar Bar = new Bar();
        public Projectile Ball = new Projectile();
        public event PropertyChangedEventHandler PropertyChanged;
        public List<Cell> Cells { get; set; }
        public int Refresh { get; set; } = 60;
        public char direction { get; set; } = 'R';
        bool isRunning { get; set; }
        public bool GameOver { get; set; } = false;
        public int Score { get; set; } = 0;
        private bool drop;

        public BreakoutManager(DesktopWindow _win)
        {
            win = _win;
        }

        void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
        }

        public void MousePos(MouseEventArgs e)
        {
            int oldPos = Bar.XPos; 
            // Bar.XPos = (int) e.OffsetX - (int)(Ball.Speed * 10);
            Bar.XPos = (int) e.OffsetX - (Bar.Width / 2);
            int vel = Bar.XPos - oldPos;
            Console.WriteLine($"{oldPos}, {Bar.XPos}");
            // vel += vel;
            Bar.Velocity = 0;
            // if(vel < 10 && vel > -10)
                Bar.Velocity = vel;
            Bar.Position = $"left:{Bar.XPos}px;";
        }

        public void Start()
        {
            direction = 'R';
            GameOver = false;
            WM.Lock = true;
            PlayWidth = WM.window.Width;
            PlayHeight = WM.window.Height;
            Ball = new Projectile();
            Cells = new List<Cell>();
            Bar.YPos = PlayHeight - 200;
            FillCells();
            Ball.XPos = new Random().Next(5, PlayWidth - 10);
            Ball.YPos = PlayHeight - 400;
            Ball.Angle = new Random().Next(-2, 2);
            Ball.YVelocity += Ball.Speed;
            Score = 0;
            Bar.Width = PlayWidth / 10;
            // RandomDir();
            // direction = 'd';
            MainLoop();
            UpdateLoop();
        }

        public void Pause()
        {
            WM.Lock = false;
        }

        public void UnPause()
        {
            WM.Lock = true;
            MainLoop();
            UpdateLoop();
        }

        public void Shoot()
        {
            
        }

        void RandomDir()
        {
            direction = "lrLR"[new Random().Next(0, 3)];
        }

        public int TickRate { get; set; } = 1;
        private bool spd;

        public async void MainLoop()
        {
            isRunning = true;
            while (isRunning)
            {
                if (!WM.Lock)
                {
                    isRunning = false;
                }

                if (Cells.Count == 0)
                    GameOver = true;
                
                MoveBall();
                // WM.PropChanged(Ball);
                PropChanged(Ball);
                if(drop && Ball.YPos >= PlayHeight - (210 + (Bar.Height * 2)) && Score != 0)
                    DropCells();
                // WM.PropChanged(WM.window);
                Ball.Position = $"left: {Ball.XPos}px; top: {Ball.YPos}px;";
                await Task.Delay(TickRate);
            }
        }

        public void SpeedUp()
        {
            TickRate = 100;
        }
        
        public void SlowDown()
        {
            TickRate = 5;
            // if(TickRate < 100)
            // if(TickRate < 100)
            //     TickRate++;
        }

        public async void UpdateLoop()
        {
            while (isRunning)
            {
                // if (Bar.Velocity < 0)
                //     Bar.Velocity += 2;
                // else if (Bar.Velocity > 0)
                //     Bar.Velocity -= 2;
                
                // WM.window.Status = $"Score: {Score}";
                // WM.window.SetStatus($"Score: {Score}");
                // WM.PropChanged(WM.window);
                win.SetStatus($"Score: {Score}");
                // WM.PropChanged(win);
                await Task.Delay(100);
            }
        }

        string SetColor(int hp) //  Sets the color based on health
        {
            string[] colors = {"green", "cyan", "blue", "red"};
            try
            {
                return colors[hp - 1];
            }
            catch
            {
                return "red";
            }
        }

        void FillCells() //  Lays out the cells in a grid, doing it this way to get the coordinates easier
        {
            int width = PlayWidth / 10; //26
            int height = PlayHeight / 30; //11
            int left = 0;
            //int bottom = PlayHeight - Theme.HeaderHeight; //580
            int bottom = 0;
            for (int i = 0; i < 98; i++)
            {
                //if (i % 14 == 0)    //  Starts new row when a row of 14 is filled. FizzBuzz useful for once!
                if (left + width > PlayWidth)
                {
                    bottom += height;
                    left = 0;
                }

                int hp = new Random().Next(1, 4);
                string css = $"width:{width}px;height:{height}px;left:{left}px; top: {bottom}px;";
                string color = $"background-color: {SetColor(hp)};";
                Cells.Add(new Cell()
                {
                    Health = hp, 
                    XPos = left, 
                    YPos = bottom, 
                    CSS = css, 
                    Color = color, 
                    Height = height, 
                    Width = width
                });
                left += width;
            }

            int rnd = new Random().Next(15, 40);
            for (int i = 0; i < rnd; i++)
            {
                int r = new Random().Next(0, Cells.Count());
                Cells.Remove(Cells[r]);
            }
        }

        void DropCells()
        {
            foreach (Cell cell in Cells)
            {
                cell.YPos += cell.Height;
                cell.CSS =  $"width:{cell.Width}px;height:{cell.Height}px;left:{cell.XPos}px; top: {cell.YPos}px;";
                if (cell.YPos >= PlayHeight - (200 + Theme.HeaderHeight))
                {
                    GameOver = true;
                }
            }
            drop = false;
        }

        void MoveBall()
        {
            CollisionCheck();
            Heading();
        }

        void CollisionCheck() //  Compares the projectile's position to potential targets
        {
            if (Ball.XPos <= 0)
            {
                SwitchRight();
                // SwitchSide();
                // switch (direction)
                // {
                //     case 'l':
                //         direction = 'r';
                //         break;
                //     case 'L':
                //         direction = 'R';
                //         break;
                // }
            }

            if (Ball.XPos >= PlayWidth - 19)
            {
                SwitchLeft();
            }

            if (Ball.YPos + Ball.Size >= PlayHeight - 200 && Ball.YPos <= PlayHeight - (200 - Bar.Height))
            {
                if(isWithin(Ball.XPos, Bar.XPos, Bar.Width))
                {
                    // Console.WriteLine("hit");
                    SwitchBar();
                    if (Bar.Velocity != 0)
                        Ball.Angle += Bar.Velocity * 0.1;
                    // else
                    //     Ball.Angle -= Ball.Angle * 0.1;
                    // Ball.Angle = (int)Math.Ceiling(Bar.Velocity * 0.1) / 2;
                    // Ball.Angle = Math.Ceiling(0.5);
                }
            }
                foreach(Cell cell in Cells.Where(c => c.YPos - 10 <= Ball.YPos + Ball.Size && c.YPos + c.Height + 10 >= Ball.YPos))
                // foreach (Cell cell in Cells.Where(c => isWithin(Ball.XPos, c.XPos, c.Width)))
                // foreach (var cell in Cells.Where(c => PE.IsColliding((int)Ball.XPos, (int)Ball.YPos, Ball.Size, Ball.Size, c.XPos, c.YPos, c.Width, c.Height)))
                {
                    if (cell.Inv <= 0)
                    {
                        // Runs like ass, but runs
                        if (PE.IsTouching((int) Ball.YPos, Ball.Size, cell.YPos + cell.Height)
                            && PE.IsInside((int) Ball.XPos, Ball.Size, cell.XPos, cell.Width))
                        {
                            SwitchDown();
                            Hit(cell, false);
                            break;
                        }
                        else if (PE.IsTouching((int) Ball.YPos, Ball.Size, cell.YPos)
                                 && PE.IsInside((int) Ball.XPos, Ball.Size, cell.XPos, cell.Width))
                        {
                            SwitchUp();
                            Hit(cell, false);
                            break;
                        }
                        else if (PE.IsTouching((int) Ball.XPos, Ball.Size, cell.XPos)
                                 && PE.IsInside((int) Ball.YPos, Ball.Size, cell.YPos, cell.Height))
                        {
                            SwitchLeft();
                            Hit(cell, false);
                            break;
                        }
                        else if (PE.IsTouching((int) Ball.XPos, Ball.Size, cell.XPos + cell.Width)
                                 && PE.IsInside((int) Ball.YPos, Ball.Size, cell.YPos, cell.Height))
                        {
                            SwitchRight();
                            Hit(cell, false);
                            break;
                        }
                    }
                    else if(cell.Inv > 0)
                    {
                        cell.Inv--;
                    }

                    // cell.Color = "background-color: Green;";
                    // if (isTouch(Ball.YPos, cell.YPos, Ball.Size))
                    // {
                    //     SwitchUp();
                    //     Hit(cell, false);
                    // }
                    // else if (isTouch(Ball.YPos, cell.YPos + cell.Height, Ball.Size))
                    // {
                    // SwitchDown();
                    //     Hit(cell, false);
                    // }
                    // if (isTouch(Ball.XPos, cell.XPos + cell.Width, Ball.Size))
                    // {
                    //     Console.WriteLine("hit side");
                    //     SwitchRight();
                    //     Hit(cell, false);
                    // }
                    // else if (isTouch(Ball.XPos, cell.XPos, Ball.Size))
                    // {
                    //     Console.WriteLine("hit side");
                    //     SwitchLeft();
                    //     Hit(cell, false);
                    // }
                    // if(isWithin(Ball.YPos, cell.YPos, cell.Height) && isOntop(Ball.XPos, cell.XPos, cell.Width))
                    // {
                    //     // Console.WriteLine("Within");
                    //     // SwitchSide();
                    //     Hit(cell, true);
                    //     break;
                    // }
                    // else if(isWithin(Ball.YPos, cell.YPos, cell.Height))
                    // {
                    //     Console.WriteLine("On top");
                    //     // SwitchUp();
                    //     Hit(cell, false);
                    //     break;
                    // }
                }

                Cells.RemoveAll(c => c.Kill);

            if (Ball.YPos <= 0)
            {
                SwitchDown();
            }
            else if (Ball.YPos >= PlayHeight - 50)
            {
                isRunning = false;
                GameOver = true;
            }
        }

        // bool isToLeft(double ball, int obstacle, int offset)
        // {
        //     if(ball <= )
        //     return false;
        // }

        bool isTouch(double ball, int obstacle, int offset)
        {
            if (ball <= obstacle && ball + offset >= obstacle)
            {
                return true;
            }
            return false;
        }

        bool isWithin(double ball, int obstacle, int offset)
        {
            // if (ball + 10 > obstacle && ball < obstacle + offset)
            if (ball > obstacle && ball + 10 < obstacle + offset)
            {
                return true;
            }
            return false;
        }
        bool isOntop(double ball, int obstacle, int offset)
        {
            if (ball + 10 >= obstacle && ball <= obstacle + offset)
            {
                return true;
            }
            return false;
        }

        void Hit(Cell cell, bool side)
        {
            if (--cell.Health <= 0)
            {
                cell.Kill = true;
            }

            cell.Color = $"background-color: {SetColor(cell.Health)};";
            Score++;
            if (Score % 10 == 0)
                drop = true;

            cell.Inv = 5;

            // if(side)
            //     SwitchSide();
            // else
            //     SwitchUp();
        }

        void SwitchDown() //  Switches direction to up from down
        {
            Ball.YVelocity = Ball.Speed - (Ball.Speed * 2);
            switch (direction)
            {
                case 'L':
                    direction = 'l';
                    break;
                case 'R':
                    direction = 'r';
                    break;
            }
        }

        void SwitchUp()
        {
            Ball.YVelocity = Ball.Speed - (Ball.Speed * 2);
            switch (direction)
            {
                case 'l':
                    direction = 'L';
                    break;
                case 'r':
                    direction = 'R';
                    break;
            }
        }

        void SwitchBar()
        {
            if (Bar.Velocity < 0)
                direction = 'L';
            else if (Bar.Velocity > 0)
                direction = 'R';
            direction = char.ToUpper(direction);
        }

        void SwitchSide()
        {
            
            // SwitchRight();
            // SwitchLeft();
            
            switch (direction)
            {
                case 'L':
                    direction = 'R';
                    break;
                case 'l':
                    direction = 'r';
                    break;
                case 'R':
                    direction = 'L';
                    break;
                case 'r':
                    direction = 'l';
                    break;
            }
        }

        void SwitchRight()
        {
            Ball.Angle = Ball.Angle - (Ball.Angle * 2);
            switch (direction)
            {
                case 'L':
                    direction = 'R';
                    break;
                case 'l':
                    direction = 'r';
                    break;
            }
        }
        void SwitchLeft()
        {
            Ball.Angle = Ball.Angle - (Ball.Angle * 2);
            switch (direction)
            {
                case 'R':
                    direction = 'L';
                    break;
                case 'r':
                    direction = 'l';
                    break;
            }
        }

        void Move()
        {
            Ball.XPos += Ball.Angle;
            Ball.YPos += Ball.YVelocity;
        }

        void BounceSide()
        {
            Ball.XVelocity -= (Ball.XVelocity * 2);
        }

        void BounceTop()
        {
            Ball.YVelocity -= (Ball.YVelocity * 2);
        }

        void Heading() //  Controls the continous direction
        {
            
            // if (Ball.Angle > Ball.Speed)
            //     Ball.Angle = Ball.Speed;
            // if (Ball.Angle < -Ball.Speed)
            //     Ball.Angle = -Ball.Speed;
            // Ball.XVelocity = (Ball.Angle * Ball.Speed);
            
            // if (Ball.Angle > 100)
            //     Ball.Angle = 100;
            // if (Ball.Angle < -100)
            //     Ball.Angle = -100;
            // if (Ball.Angle == 0)
            //     Ball.Angle = Ball.Direction;
            // Ball.Direction = Ball.Angle;
            // Ball.XVelocity = Ball.Speed * (Ball.Direction * 0.1);
            
            // Ball.YVelocity = Ball.Speed - Ball.XVelocity;
            // Ball.Angle = Ball.XVelocity + Ball.YVelocity;
            // BounceSide();
            
            // Move();

            if (Ball.Angle > 2.5)
                Ball.Angle = 2.5;
            if (Ball.Angle < -2.5)
                Ball.Angle = -2.5;
            
            switch (direction)
            {
                case 'd':
                    Down();
                    break;
                case 'u':
                    Up();
                    break;
                case 'l':
                    Down();
                    Left();
                    break;
                case 'L':
                    Up();
                    Left();
                    break;
                case 'r':
                    Down();
                    Right();
                    break;
                case 'R':
                    Up();
                    Right();
                    break;
            }
        }

        void Up()
        {
            Ball.YPos -= Ball.Speed;
        }

        void Down()
        {
            Ball.YPos += Ball.Speed;
        }

        void VertiDir()
        {
            Ball.YPos += Ball.Speed;
        }

        void SideDir()
        {
            Ball.XPos += Ball.Angle;
        }

        void Left()
        {
            // if (Ball.Angle < 0)
            //     Ball.XPos -= Ball.Angle;
            // else if (Ball.Angle > 0)
            //     Ball.XPos -= Ball.Angle - (Ball.Angle * 2);
            
            // Ball.XPos -= Ball.Speed;
            // int vel = Ball.XPos - Ball.Angle;
            // if (vel > Ball.XPos)
            //     Ball.XPos -= Ball.Angle;
            // else if(vel < Ball.XPos)
            Ball.XPos += Ball.Angle;
        }

        void Right()
        {
            // if (Ball.Angle < 0)
            //     // Ball.XPos -= Ball.Angle;
            //     Ball.XPos -= Ball.Angle - (Ball.Angle * 2);
            // else if (Ball.Angle > 0)
            //     Ball.XPos += Ball.Angle;
                // Ball.XPos -= Ball.Angle - (Ball.Angle * 2);
            // int vel = Ball.XPos - Ball.Angle;
            // if (vel > Ball.XPos)
            //     Ball.XPos += Ball.Angle;
            // else if(vel < Ball.XPos)
            Ball.XPos += Ball.Angle;
            // Ball.XPos += Ball.Speed;
        }
    }
}