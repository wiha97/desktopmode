﻿namespace DesktopMode.Components.Apps.Blazeout.Models
{
    public class Bar
    {
        public int XPos { get; set; }
        public int YPos { get; set; }
        public string Position { get; set; }
        public double Velocity { get; set; }
        public int Width { get; set; } = 50;
        public int Height { get; set; } = 10;

        public string CSS()
        {
            return $"width: {Width}px; height: {Height}px; top: {YPos}px;";
        }
    }
}
