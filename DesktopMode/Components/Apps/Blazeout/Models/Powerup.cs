namespace DesktopMode.Components.Apps.Blazeout.Models;

public class Powerup
{
    public string Name { get; set; }
    public string Ability { get; set; }
    public int XPos { get; set; }
    public int YPos { get; set; }
    public int Width { get; set; } = 10;

    public string CSS()
    {
        return $"left: {XPos}px; top: {YPos}px;";
    }
}

public class Laser : Powerup
{
    public Laser()
    {
        Name = "Laser";
    }
}