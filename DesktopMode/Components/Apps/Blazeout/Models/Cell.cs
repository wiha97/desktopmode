﻿namespace DesktopMode.Components.Apps.Blazeout.Models
{
    public class Cell
    {
        public int Health { get; set; }
        public int XPos { get; set; }
        public int YPos { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string CSS { get; set; }
        public string Color { get; set; }
        public bool Kill { get; set; }
        public int Inv { get; set; }
    }
}
