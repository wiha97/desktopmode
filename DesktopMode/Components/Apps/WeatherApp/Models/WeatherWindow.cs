using DesktopMode.Models;

namespace DesktopMode.Components.Apps.WeatherApp.Models;

public class WeatherWindow : DesktopWindow
{
    public WeatherWindow()
    {
        Name = "weather";
        Width = 270;
        Height = 323;
        Icon = $"assets/aboutme.png";
        Type = WinType.App;
        SetParam();
    }
}