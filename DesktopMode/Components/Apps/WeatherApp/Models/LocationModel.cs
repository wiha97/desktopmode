using System.Text.Json.Serialization;

namespace DesktopMode.Components.Apps.WeatherApp.Models;

public class LocationModel
{
    [JsonPropertyName("name")]
    public string City { get; set; }
    [JsonPropertyName("lat")]
    public double Lat { get; set; }
    [JsonPropertyName("lon")]
    public double Lon { get; set; }
}