using System.Text.Json.Serialization;

namespace DesktopMode.Components.Apps.WeatherApp.Models;

public class WeatherModel
{
    [JsonPropertyName("weather")]
    public List<WeatherInfo> Info { get; set; } = new();
    [JsonPropertyName("main")]
    public WeatherMain Main { get; set; } = new();
    [JsonPropertyName("wind")]
    public WindModel Wind { get; set; } = new();
    [JsonPropertyName("sys")]
    public SysModel Sys { get; set; }

    public string RealTemp()
    {
        return Celsius(Main.Temp);
    }
    public string FeelTemp()
    {
        return Celsius(Main.FeelsLike);
    }
    public string TempMin()
    {
        return Celsius(Main.TempMin);
    }
    public string TempMax()
    {
        return Celsius(Main.TempMax);
    }

    public string SunRise()
    {
        return ConDate(Sys.SunRise).ToString("HH:mm");
    }
    public string SunSet()
    {
        return ConDate(Sys.SunSet).ToString("HH:mm");
    }

    private string Celsius(double temp)
    {
        return $"{temp}°C";
    }

    private DateTime ConDate(int unix)
    {
        return DateTimeOffset.FromUnixTimeSeconds(unix).UtcDateTime.ToLocalTime();
        // return DateTime.FromBinary(unix);
    }

    public string WindSpeed()
    {
        return $"{Wind.Speed} m/h";
    }

    public string Icon(string icon)
    {
        return $"https://openweathermap.org/img/wn/{icon}.png";
    }
}

public class WeatherInfo
{
    [JsonPropertyName("description")] 
    public string Description { get; set; } = "Something here";
    [JsonPropertyName("icon")]
    public string Icon { get; set; } = "";
}

public class WeatherMain
{
    [JsonPropertyName("temp")] 
    public double Temp { get; set; } = 3.14;
    [JsonPropertyName("feels_like")]
    public double FeelsLike { get; set; } = 3.14;
    [JsonPropertyName("temp_min")]
    public double TempMin { get; set; }
    [JsonPropertyName("temp_max")]
    public double TempMax { get; set; }
    [JsonPropertyName("pressure")]
    public double Pressure { get; set; }
    [JsonPropertyName("humidity")]
    public double Humidity { get; set; }
}

public class WindModel
{
    [JsonPropertyName("speed")]
    public double Speed { get; set; } = 3.14;
    [JsonPropertyName("deg")]
    public double Degree { get; set; }
    [JsonPropertyName("gust")]
    public double Gust { get; set; }
}

public class SysModel
{
    [JsonPropertyName("sunrise")]
    public int SunRise { get; set; }
    [JsonPropertyName("sunset")]
    public int SunSet { get; set; }
}