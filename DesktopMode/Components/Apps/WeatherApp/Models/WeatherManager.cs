using System.Net.Http.Json;
using System.Text.Json;
using DesktopMode.Models;

namespace DesktopMode.Components.Apps.WeatherApp.Models;

public class WeatherManager
{
    public WeatherModel Weather { get; set; } = new();
    private LocationModel _City = new();

    public async Task<string> GetWeather(string city)
    {
        await GetCity(city);
        string url = $"{API.Proxy}/weather?lat={_City.Lat}&lon={_City.Lon}";

        HttpClient client = new();
        try
        {
            Weather = await client.GetFromJsonAsync<WeatherModel>(url).WaitAsync(TimeSpan.FromSeconds(5));
            return $"Weather in {_City.City}";
        }
        catch (TimeoutException)
        {
            return "Timed out";
        }
        catch (Exception e)
        {
            Console.WriteLine($"Failed weather: {e.Message}\n{e}");
            return $"ERROR: {e.Message}";
        }
    }

    async Task GetCity(string city)
    {
        string url = $"{API.Proxy}/city?city={city}";
        List<LocationModel> cities;

        HttpClient client = new();
        try
        {
            cities = await client.GetFromJsonAsync<List<LocationModel>>(url).WaitAsync(TimeSpan.FromSeconds(5));
            _City = cities[0];
        }
        catch (TimeoutException)
        {
            Console.WriteLine("Timed out");
        }
        catch (Exception e)
        {
            Console.WriteLine($"Failed city: {e.Message}\n{e}");
        }
    }
}