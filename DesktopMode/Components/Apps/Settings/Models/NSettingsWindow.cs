using DesktopMode.Models;

namespace DesktopMode.Components.Apps.Settings.Models;

public class NSettingsWindow : DesktopWindow
{
    public NSettingsWindow()
    {
        Name = "settingsmain";
        Width = 670;
        Height = 550;
        Type = WinType.Help;
    }
}