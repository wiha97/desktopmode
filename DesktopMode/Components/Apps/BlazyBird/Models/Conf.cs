﻿namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class Conf
    {
        //  Game Logic
        public static int Gravity { get; set; } = 2;
        public static int Speed { get; set; } = 1;
        public static int Blaze { get; set; } = 50;

        //  game-container
        public static int GameWidth { get; set; } = 370;    //  Mobile friendly, 600 on PC
        public static int GameHeight { get; set; } = 730;
        //  sky
        public static int SkyWidth { get; set; } = 370;     //  Mobile friendly, 600 on PC
        public static int SkyHeight { get; set; } = 580;
        //  ground
        public static int GroundWidth { get; set; } = 370;  //  Mobile friendly, 600 on PC
        public static int GroundHeight { get; set; } = 150;
        public static int GroundTop { get; set; } = 580;
        //  bird
        public static int BirdSize { get; set; } = 45;
        public static int BirdLeft { get; set; } = 110;     //  Mobile friendly, 220 on PC
        //  playground
        public static int PlayHeight { get; set; } = 580;
        public static int PlayWidth { get; set; } = 2100;
        //  obstacle
        public static int ObsWidth { get; set; } = 50;
        public static int ObsHeight { get; set; } = 580;
        //  backimg
        public static int ImgWidth { get; set; } = 350;
        public static int ImgHeight { get; set; } = 290;
        public static int ImgMargLeft { get; set; } = 175;
        public static int ImgMargTop { get; set; } = 145;
        //  pipes
        public static int TopWidth { get; set; } = 50;
        public static int BotWidth { get; set; } = 50;
        public static int PipeMaxHeight { get; set; } = 450;
        public static int PipeMinHeight { get; set; } = 400;

        public static string Width(int i)
        {
            return $"width: {i}px;";
        }
        public static string Height(int i)
        {
            return $"height: {i}px;";
        }
        public static string Left(int i)
        {
            return $"left: {i}px;";
        }
        public static string Top(int i)
        {
            return $"top: {i}px;";
        }
        public static string MarginLeft(int i)
        {
            return $"margin-left: {i}px;";
        }
        public static string MarginTop(int i)
        {
            return $"margin-top: {i}px;";
        }

        public static void PC()
        {
            //GameWidth = 500;
            //GameHeight = 730;

            SkyWidth = 700;
            SkyHeight = 580;

            GroundWidth = 700;
            GroundHeight = 150;
            GroundTop = 580;

            BirdSize = 45;
            BirdLeft = 220;

            PlayWidth = 2100;
            PlayHeight = 580;

            ObsHeight = 580;
            ObsWidth = 50;

            ImgWidth = 350;
            ImgHeight = 290;

            TopWidth = 50;
            BotWidth = 50;

            PipeMaxHeight = 450;
            PipeMinHeight = 400;
        }
        public static void Phone()
        {
            //GameWidth /= 2;
            //GameHeight /= 2;

            SkyWidth /= 2;
            SkyHeight /= 2;

            GroundWidth /= 2;
            GroundHeight /= 2;
            GroundTop /= 2;

            BirdSize /= 2;
            BirdLeft /= 2;

            PlayWidth /= 2;
            PlayHeight /= 2;

            ObsHeight /= 2;
            ObsWidth /= 2;

            ImgWidth /= 2;
            ImgHeight /= 2;

            TopWidth /= 2;
            BotWidth /= 2;

            PipeMaxHeight /= 2;
            PipeMinHeight /= 2;
        }
    }
}
