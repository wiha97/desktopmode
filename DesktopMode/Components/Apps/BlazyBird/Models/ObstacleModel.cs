﻿namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class ObstacleModel
    {
        public int Position { get; set; }
        public int Offset { get; set; }

        public PipeModel TopPipe;
        public PipeModel BottomPipe;

        public ObstacleModel()
        {
            Position = Conf.SkyWidth;
            TopPipe = new PipeModel();
            BottomPipe = new PipeModel();
            while ( TopPipe.Height + BottomPipe.Height > Conf.PipeMaxHeight || 
                    TopPipe.Height + BottomPipe.Height < Conf.PipeMinHeight)
            {
                TopPipe.Height = RndInt();
                BottomPipe.Height = RndInt();
            }
            //Offset = offset;
        }

        int RndInt()
        {
            return new Random().Next(0, 600);
        }

        public void Move(int speed)
        {
            Position -= speed;
        }
    }
}
