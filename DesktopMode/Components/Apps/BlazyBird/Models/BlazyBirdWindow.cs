using DesktopMode.Models;

namespace DesktopMode.Components.Apps.BlazyBird.Models;


public class BlazyBirdWindow : DesktopWindow
{
    public BlazyBirdWindow()
    {
        Name = "blazybird";
        Type = WinType.App;
        Width = 420;
        Height = 730;
        Icon = $"assets/blazybird.png";
        SetParam();
    }
}