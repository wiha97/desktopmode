﻿using System.ComponentModel;
using DesktopMode.Models;

namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class GameManager : INotifyPropertyChanged
    {
        public int MaxHeight { get; set; } = 580;

        public event PropertyChangedEventHandler PropertyChanged;
        public BirdModel Bird { get; set; }
        public List<PlaygroundModel> Plays { get; set; } = new List<PlaygroundModel>();
        public List<ObstacleModel> Obs { get; set; } = new List<ObstacleModel>();
        public bool IsRunning { get; set; } = false;
        public string Text { get; set; } = "Score: ";
        public int Fuel { get; set; } = 0;

        public int Score { get; set; } = 0;

        public DesktopWindow? win { get; set; }

        public GameManager(DesktopWindow? window)
        {
            win = window;
            Bird = new BirdModel();
            NewObstacle();
        }

        public async void UpdateLoop()
        {
            while (true)
            {
                // WM.PropChanged(WM.window);
                PropChanged(Bird);
                Console.WriteLine("updated");
                await Task.Delay(100);
            }
        }

        public async void MainLoop()
        {
            IsRunning = true;
            while (IsRunning)
            {
                if (Fuel > 0)
                {
                    if (Bird.Height < MaxHeight - Conf.Gravity)
                    {
                        int blaze = Conf.Gravity;
                        if(Fuel < 20) { blaze /= 2; }
                        Bird.Fall(-blaze);
                    }
                    Fuel--;
                }
                else
                { 
                    Bird.Fall(Conf.Gravity);
                }
                try
                {
                    NewObstacle();
                }
                catch (Exception e)
                {
                    Text = e.Message;
                }
                //PropChanged(Bird);
                foreach (ObstacleModel obs in Obs)
                {
                    obs.Move(Conf.Speed);

                    int center = Conf.BirdLeft;

                    for (int i = 0; i < Obs.Count; i++)
                    {
                        if (obs.Position + obs.Offset == center)
                        {
                            Score++;
                            if (obs.BottomPipe.Height >= Bird.Height || obs.TopPipe.Height >= MaxHeight - Bird.Height)
                            {
                                GameOver();
                            }
                        }
                    }
                    PropChanged(obs);
                }
                if (Bird.Height <= 0)
                {
                    GameOver();
                }
                Text = $"Score: {Score}";
                try
                {
                    WM.window.Status = Text;
                }
                catch{}

                //WM.PropChanged(WM.window);
                await Task.Delay(1);
            }
        }

        void NewObstacle()
        {
            if (Obs.Count > 0)
            {
                if (Obs.FirstOrDefault().Position < 0 - Conf.ObsWidth && Obs.Count < 2)
                {
                    Obs.Add(new ObstacleModel());
                    Obs.Remove(Obs.FirstOrDefault());
                }
                //if (Obs.FirstOrDefault().Position < -(Conf.PlayWidth))
                //{
                //    Obs.Remove(Obs.FirstOrDefault());
                //}
            }
            else
            {
                Obs.Add(new ObstacleModel());
            }
        }

        //void NewPlayground()
        //{
        //    if (Plays.Count > 0)
        //    {
        //        if (Plays.FirstOrDefault().Position < -(Conf.PlayWidth * 0.66) && Plays.Count < 2)
        //        {
        //            Plays.Add(new PlaygroundModel());
        //        }
        //        if (Plays.FirstOrDefault().Position < -(Conf.PlayWidth))
        //        {
        //            Plays.Remove(Plays.FirstOrDefault());
        //        }
        //    }
        //    else
        //    {
        //        Plays.Add(new PlaygroundModel());
        //    }
        //}

        void Clear()
        {
            Obs.Clear();
        }

        void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
        }

        public void BoostBird()
        {
            Fuel = Conf.Blaze;
        }

        public void Tap()
        {
            if (!IsRunning)
            {
                Bird = new BirdModel();
                Clear();
                NewObstacle();
                MainLoop();
                UpdateLoop();
                try
                {
                    WM.Lock = true;
                }
                catch{}
            }
            else
            {
                Fuel = Conf.Blaze;
            }
        }

        public void GameOver()
        {
            Text = "Game over, man! GAME OVER!";
            Score = 0;
            IsRunning = false;
            try
            {
                WM.Lock = false;
            }
            catch{}
        }
    }
}
