﻿namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class BirdModel
    {
        public int Height { get; set; } = 300;

        public void Fall(int gravity)
        {
            Height -= gravity;
        }

        public void Blaze(int strength)
        {
            Height += 1;
        }
    }
}
