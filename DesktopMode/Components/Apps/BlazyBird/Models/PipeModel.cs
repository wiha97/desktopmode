﻿namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class PipeModel
    {
        public int Height { get; set; } = 50;
        public int Position { get; set; } = 0;

        public void SetHeight(int height)
        {
            Height = height;
        }
    }
}
