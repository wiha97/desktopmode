﻿namespace DesktopMode.Components.Apps.BlazyBird.Models
{
    public class PlaygroundModel
    {
        public int Position { get; set; } = 700;

        int space = 700;

        public List<ObstacleModel> Obstacles { get; set; } = new List<ObstacleModel>();

        public PlaygroundModel()
        {
            AddObstacles();
        }

        public void AddObstacles()
        {
            int oldSpace = 0;
            while (Obstacles.Count < 3)
            {
                oldSpace += space;
                //Obstacles.Add(new ObstacleModel(oldSpace));
            }
        }

        public void Move(int speed)
        {
            Position -= speed;
        }
    }
}
