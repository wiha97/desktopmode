using DesktopMode.Models;

namespace DesktopMode.Components.Apps.BlazeOS.Models;

public class BlazeOSWindow : DesktopWindow
{
    public BlazeOSWindow()
    {
        Name = "blazeos";
        Width = 550;
        Height = 700;
        Icon = $"assets/help.png";
        Type = WinType.App;
    }
}