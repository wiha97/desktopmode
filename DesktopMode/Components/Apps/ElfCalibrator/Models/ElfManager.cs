namespace DesktopMode.Components.Apps.ElfCalibrator.Models;

public class ElfManager
{

    public int Total { get; set; }
    public bool Literally { get; set; }
    public bool ShowNum { get; set; }
    public List<string> GetCalibration(string file)
    {
        Total = 0;
        string[] lines = file.Split("\n");
        List<string> newLines = new();
        foreach (string line in lines)
        {
            string word = line;
            if (Literally)
                word = ParseLiterals(line);
            
            int i = GetNum(word);
            Total += i;
            if(ShowNum)
                newLines.Add($"{i} - {word}");
            else
                newLines.Add(word);
        }

        return newLines;
    }

    private string ParseLiterals(string word)
    {
        return word
            .Replace("one", "(1)")
            .Replace("two", "(2)")
            .Replace("three", "(3)")
            .Replace("four", "(4)")
            .Replace("five", "(5)")
            .Replace("six", "(6)")
            .Replace("seven", "(7)")
            .Replace("eight", "(8)")
            .Replace("nine", "(9)");
    }

    private int GetNum(string word)
    {
        int num = 0;

        string nums = string.Concat(word.Where(Char.IsDigit));
        char first = nums[0];
        char last = nums[nums.Length - 1];

        num = int.Parse($"{first}{last}");

        return num;
    }
}