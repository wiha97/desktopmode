using DesktopMode.Models;

namespace DesktopMode.Components.Apps.ElfCalibrator.Models;

public class ElfWindow : DesktopWindow
{
    public ElfWindow()
    {
        Name = "ElfCalibrator";
        Width = 445;
        Height = 700;
        Icon = $"assets/elf.png";
        Type = WinType.App;
        SetParam();
    }
}