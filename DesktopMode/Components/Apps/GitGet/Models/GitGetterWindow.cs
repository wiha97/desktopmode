using DesktopMode.Models;

namespace DesktopMode.Components.Apps.GitGet.Models;

public class GitGetterWindow : DesktopWindow
{
    public GitGetterWindow()
    {
        Name = "gitupdater";
        Width = 380;
        Height = 740;
        Type = WinType.App;
        Icon = $"assets/git.png";
    }
}