using System.Net.Http.Json;
using System.Text.Json;

namespace DesktopMode.Components.Apps.GitGet.Models;

public class ReleaseManager
{
    public List<ReleaseModel> _releases { get; set; }
    private static string response;

    public ReleaseManager()
    {
    }

    public async Task<string> CheckUpdates(string url)
    {
        try
        {
            url = APIurl(url);
            if (url.Contains("github"))
            {
                HttpClient client = new();
                _releases = await client.GetFromJsonAsync<List<ReleaseModel>>(url);
            }

            return _releases[0].Tag;
        }
        catch (Exception e)
        {
            Console.WriteLine($"Failed update check\n{url}\n{e.Message}");
            Console.WriteLine(e);
            return e.Message;
        }
    }

    private string APIurl(string url)
    {
        if (!url.Contains("github.com"))
        {
            url = $"https://github.com/{url}";
        }
        
        string newUrl = url.Replace("//github.com", "//api.github.com/repos");
        return $"{newUrl}/releases";
    }

    public List<ReleaseModel> Releases()
    {
        return _releases;
    }
}