using DesktopMode.Models;

namespace DesktopMode.Components.Apps.Calendar.Models;

public class CalendarWindow : DesktopWindow
{
    public CalendarWindow()
    {
        Name = "calendar";
        Width = 270;
        Height = 250;
        // Icon = $"assets/calendar.png";
        Type = WinType.Widget;
    }
}