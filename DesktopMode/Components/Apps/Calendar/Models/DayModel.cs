using System.Globalization;

namespace DesktopMode.Components.Apps.Calendar.Models;

public class DayModel
{
    public int WeekDay { get; set; }
    public int Day { get; set; }
    public int Week { get; set; }

    public DayModel(DateTime dateTime)
    {
        WeekDay = (int)dateTime.DayOfWeek;
        Day = dateTime.Day;
        Week = ISOWeek.GetWeekOfYear(dateTime);
    }
}