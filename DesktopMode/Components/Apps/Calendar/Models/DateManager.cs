namespace DesktopMode.Components.Apps.Calendar.Models;

public class DateManager
{
    private List<DayModel> _days = new();
    private List<int> _weeks = new();

    public List<DayModel> Days
    {
        get { return _days; }
        set { _days = value; }
    }

    public List<int> Weeks
    {
        get { return _weeks; }
        set { _weeks = value; }
    }
    
    public DateManager()
    {
        DateTime dt = new DateTime(2024, 01, 01);
        int dim = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        for (int i = 1; i <= dim; i++)
        {
            DateTime thisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i);
            DayModel day = new DayModel(thisMonth);
            _days.Add(day);
            if(!_weeks.Contains(day.Week))
                _weeks.Add(day.Week);
        }
        
    }
}