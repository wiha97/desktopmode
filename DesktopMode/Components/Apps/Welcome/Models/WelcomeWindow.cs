using DesktopMode.Models;

namespace DesktopMode.Components.Apps.Welcome.Models;

public class WelcomeWindow : DesktopWindow
{
    public WelcomeWindow()
    {
        Name = "welcome";
        YeetAble = true;
        Icon = "assets/help.png";
        Type = WinType.Help;
        SetParam();
    }
}