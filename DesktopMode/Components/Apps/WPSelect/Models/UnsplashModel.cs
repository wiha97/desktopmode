using System.Text.Json.Serialization;

namespace DesktopMode.Components.Apps.WPSelect.Models;

public class UnsplashSearch
{
    [JsonPropertyName("results")] 
    public List<UnsplashModel> Images { get; set; } = new();
}
public class UnsplashModel
{
    [JsonPropertyName("id")]
    public string ID { get; set; }
    
    [JsonPropertyName("urls")]
    public UnsplashUrls Url { get; set; }
    
    [JsonPropertyName("links")]
    public UnsplashLinks Links { get; set; }
    
    [JsonPropertyName("user")]
    public UnsplashUser User { get; set; }
    
}

public class UnsplashUrls
{
    [JsonPropertyName("raw")]
    public string Raw { get; set; }
    [JsonPropertyName("thumb")]
    public string Thumb { get; set; }
}

public class UnsplashLinks
{
    [JsonPropertyName("download_location")]
    public string DownloadLocation { get; set; }
}

public class UnsplashUser
{
    [JsonPropertyName("id")]
    public string ID { get; set; }
    
    [JsonPropertyName("name")]
    public string FullName { get; set; }
    
    [JsonPropertyName("username")]
    public string UserName { get; set; }
    
    [JsonPropertyName("first_name")]
    public string FirstName { get; set; }
    
    [JsonPropertyName("last_name")]
    public string LastName { get; set; }
}