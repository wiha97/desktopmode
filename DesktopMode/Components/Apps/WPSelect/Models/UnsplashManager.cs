using System.Net.Http.Json;
using System.Text.Json;
using DesktopMode.Models;

namespace DesktopMode.Components.Apps.WPSelect.Models;

public class UnsplashManager
{
    public string Query { get; set; } = "galaxies";
    public UnsplashSearch Images { get; set; } = new();
    public bool Searching { get; set; }
    public async Task GetImages()
    {
        string url = $"{API.Proxy}/unsplash?query={Query}";
        try
        {
            Searching = true;
            HttpClient http = new();
            Images = await http.GetFromJsonAsync<UnsplashSearch>(url);
            Searching = false;
        }
        catch (Exception e)
        {
            Console.WriteLine($"{url}\n{e}");
        }
    }
}