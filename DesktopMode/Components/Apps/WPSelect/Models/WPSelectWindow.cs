using DesktopMode.Models;

namespace DesktopMode.Components.Apps.WPSelect.Models;

public class WPSelectWindow : DesktopWindow
{
    public WPSelectWindow()
    {
        Name = "wpselect";
        Width = 682;
        Height = 460;
        Type = WinType.Popup;
    }
}