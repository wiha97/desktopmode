﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models.Memory
{
    public class CardModel
    {
        public int ID { get; set; }
        public string Color { get; set; }
        public bool Flipped { get; set; }
        public bool Paired { get; set; }
    }
}
