﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DesktopMode.Models.Memory
{
    public class MemManager
    {
        public List<CardModel> Cards { get; set; } = new List<CardModel>();
        public int Score { get; set; } = 0;
        public string Status { get; set; } = "";
        public string StatCol { get; set; } = "white";
        string[] colors = { "red", "blue", "green", "orange", "gray", "pink", "yellow", "purple" };

        public MemManager()
        {
            AddCards();
        }

        public void NewGame()
        {
            Cards.Clear();
            Score = 0;
            Status = "Started new game";
            AddCards();
        }

        void GameOver(bool win)
        {
            if (win)
            {
                StatCol = "white";
                Status = "You win!";
            }
            else
            {
                StatCol = "red";
                Status = "Game over!";
            }
        }

        public void FlipCard(CardModel card)
        {
            //List<CardModel> cards = Cards.Where(c => c.Flipped).ToList();
            if (Cards.Where(c => c.Flipped).Count() < 2)
            {
                if (!card.Paired)
                {
                    card.Flipped = true;
                    //Console.WriteLine(card.ID);
                }
                List<CardModel> cards = Cards.Where(c => c.Flipped).ToList();
                if (cards.Count == 2)
                {
                    string color = cards[0].Color;
                    if (color == cards[1].Color)
                    {
                        Status = $"{color} paired";
                        StatCol = color;
                        foreach (CardModel c in cards)
                        {
                            c.Flipped = false;
                            c.Paired = true;
                            //Console.WriteLine($"Paired {card.ID}");
                        }
                        Score++;
                    }
                }
            }
            else
            {
                card.Flipped = true;
                foreach (CardModel c in Cards.Where(c => c != card))
                {
                    c.Flipped = false;
                    //Console.WriteLine($"flipped {c.ID}");
                }
            }

            if (Score == Cards.Count() / 2)
            {
                GameOver(true);
            }
        }

        public string FlipCheck(CardModel card)
        {
            string color = "darkgray";

            if (card.Flipped)
            {
                color = card.Color;
            }
            if (card.Paired)
                color = "black";

            return color;
        }

        public void Print(string text)
        {
            Console.WriteLine(text);
        }

        public void CompareCards(CardModel card)
        {
            List<CardModel> cards = Cards.Where(c => c.Flipped).ToList();
            if (cards.Count == 2)
            {
                if (cards[0].Color == cards[1].Color)
                {
                    Score++;
                    foreach (CardModel c in cards)
                    {
                        c.Paired = true;
                        c.Flipped = false;
                    }
                }
                else
                {
                    foreach (CardModel c in cards)
                        c.Flipped = false;
                }
            }
        }

        public void Shuffle()
        {
            Random rnd = new Random();
            Cards = Cards.OrderBy(c => rnd.Next(0, 15)).ToList();
        }

        void AddCards()
        {
            int id = 0;
            int colorId = 0;
            while (Cards.Count() < 16)
            {
                Cards.Add(new CardModel { Color = colors[colorId], ID = id++ });
                if (Cards.Where(c => c.Color == colors[colorId]).Count() == 2)
                    colorId++;
            }
            Shuffle();
        }
    }
}
