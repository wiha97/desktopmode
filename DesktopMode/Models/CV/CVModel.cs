namespace DesktopMode.Models.CV;

public class CVModel
{
    public string Name { get; set; }
    public string Title { get; set; }
    public string Profile { get; set; }
    public List<ContactInfo> Contact { get; set; }
    public List<Job> Jobs { get; set; }
    public List<string> Skills { get; set; }
    public List<string> SoftSkills { get; set; }
    public List<Education> Educations { get; set; }
    public List<Project> Projects { get; set; }
}

public class ContactInfo
{
    public string Info { get; set; }
    public string? Link { get; set; }
}

public class Job
{
    public string Start { get; set; }
    public string End { get; set; }
    public string Title { get; set; }
    public string Company { get; set; }
    public List<string> Highlights { get; set; }

    public Job(string start, string end, string title, string company, List<string> highlights)
    {
        Start = start;
        End = end;
        Title = title;
        Company = company;
        Highlights = highlights;
    }
}

public class Project
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string Link { get; set; }
    public List<string> Tech { get; set; }

    public Project(string name, string description, string link, List<string> tech)
    {
        Name = name;
        Description = description;
        Link = link;
        Tech = tech;
    }
}
public class Education
{
    public string Start { get; set; }
    public string End { get; set; }
    public string Title { get; set; }
    public string School { get; set; }

    public Education(string start, string end, string title, string school)
    {
        Start = start;
        End = end;
        Title = title;
        School = school;
    }
}

