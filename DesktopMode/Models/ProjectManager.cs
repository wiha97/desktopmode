﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models
{
    public class ProjectManager
    {
        public List<ProjectModel> Projects { get; set; } = new List<ProjectModel>();

        public ProjectManager()
        {
            Projects.Add(
                new ProjectModel()
                {
                    Title = "Blazy Bird",
                    Description = "Flappy Bird klon byggd i Blazor",
                    Tech = new string[] { "C#", "Blazor", ".NET 5" },
                    Git = @"https://www.github.com/wiha97/blazybird",
                    Img = @"assets\blazybird.png"
                });
            Projects.Add(
                new ProjectModel()
                {
                    Title = "Ark DX",
                    Description = "Filhanterare för Halo: Master Chief Collection som automatiskt sparar statistik och repris filer",
                    Tech = new string[] { "C#", "WPF", ".NET 4.7" },
                    Git = @"https://www.github.com/wiha97/arkdx",
                    Img = @"assets\arkdx.png"
                });
            Projects.Add(
                new ProjectModel()
                {
                    Title = "ChatApp",
                    Description = "Chatt applikation byggd med Avalonia UI och .NET 5 för Linux support",
                    Tech = new string[] { "C#", "Avalonia UI", "Sockets", ".NET 5" },
                    Git = @"https://www.github.com/wiha97/chatapp",
                    Img = @"assets\chatapp.png"
                });
            Projects.Add(
                new ProjectModel()
                {
                    Title = "Autosorter",
                    Description = "Filhanterare som automatiskt sorterar filer i en mapp beroende på vilken process som körs",
                    Tech = new string[] { "C#", "WPF", ".NET 4" },
                    Git = @"https://www.github.com/wiha97/autosorter",
                    Img = @"assets\autosorter.png"
                });
        }
    }
}
