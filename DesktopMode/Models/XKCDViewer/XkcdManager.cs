﻿using System.Text.Json;

namespace DesktopMode.Models.XKCDViewer
{
    public class XkcdManager
    {
        public XkcdModel? Xkcd;
        public int CN { get; set; } = 0;

        public XkcdManager()
        {
            GetComic(42);
        }

        public void GetComic(int cn)
        {
            try
            {
                CN = cn;
                //CN = new Random().Next();
                string url = $@"https://thingproxy.freeboard.io/fetch/https://xkcd.com/{CN}/info.0.json";

                string json = "";
                HttpClient client = new HttpClient();

                json = client.GetStringAsync(url).Result;

                Xkcd = JsonSerializer.Deserialize<XkcdModel>(json);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
