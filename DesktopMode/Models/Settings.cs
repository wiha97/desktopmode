﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models
{
    public static class Theme
    {
        public static string TestBorder { get; set; } = "border: 1px dashed yellow;";

        public static int XResolution { get; set; } = 1920; //6348;
        public static int YResolution { get; set; } = 1080; //3276;
        // public static int XResolution { get; set; } = 2560;
        // public static int YResolution { get; set; } = 1440;
        public static bool PanGlass { get; set; } = false;
        public static bool AccGlass { get; set; } = false;
        //public static bool BodyGlass { get; set; } = false;
        public static bool WinGlass { get; set; } = false;
        public static string[] Wallpapers { get; set; } = { $@"assets/wallpapers/kde-plasma-flow.jpg", $@"assets/wallpapers/kde-safelanding.jpg", $@"assets/wallpapers/andy-holmes-space.jpg", $@"assets/wallpapers/cp2077.jpg", $@"assets/wallpapers/rdr2.jpg" };
        public static string[] Alignment { get; set; } = { "left", "center", "right" };
        public static List<PanelDock> DockPositions { get; set; } = new();
        public static string TitleAlign { get; set; } = Alignment[1];
        public static int Radius { get; set; } = 8;
        public static int HeaderHeight { get; set; } = 26;
        public static int SideBorder { get; set; } = 1;
        public static int TipBorder { get; set; } = 1;
        public static int HeaderBlur { get; set; } = 8;
        public static int BodyBlur { get; set; } = 8;
        public static double TaskOp { get; set; } = 5;
        public static double HeadOp { get; set; } = 7;
        public static double BodyOp { get; set; } = 8;
        public static string BG { get; set; } = /*"#1d1515";*/ "#252525";
        public static string FG { get; set; } = "#FFFFFF";
        public static string Accent { get; set; } = "#323232" /*"#0593EB"*/ /*"#00008b"*/;
        public static string? Wallpaper { get; set; } = Wallpapers[2];
        public static string DesktopBackground { get; set; } = "#78C0F0";
        public static double Skew { get; set; } = 1;
        public static bool Wobble { get; set; } = true;
        public static bool YeetMode { get; set; }
        public static bool WPShow { get; set; } = true;
        
        public static void ShowWP(bool show)
        {
            WPShow = show;
        }
        public static string Blur(int blur)
        {
            return $"backdrop-filter: blur({blur}px);";
        }
        public static string PanelBG()
        {
            return Background(Accent, PanGlass);
        }
        public static string WinAccBG()
        {
            return Background(Accent, AccGlass);
        }
        public static string WinBG()
        {
            return Background(BG, WinGlass);
        }
        public static string MiscBG(string bg)
        {
            return Background(bg, false);
        }
        public static string Background(string type, bool blur)
        {
            if (blur)
                return $"background-color: {null};";
            else
                return $"background-color: {type};";
        }
        public static string Foreground(string type)
        {
            return $"color: {type};";
        }

        public static string Border(int width, int height, string color, string type)
        {
            return $"border-top: {height}px {color} {type};" +
                   $"border-bottom: {height}px {color} {type};" +
                   $"border-left: {width}px {color} {type};" +
                   $"border-right: {width}px {color} {type};";
        }
        public static string Outline(int width, string color, string type)
        {
            return $"outline: {width}px {color} {type};";
        }
        public static string Opacity(double opacity)
        {
            return $"opacity:{(double)opacity / 10};";
            //return (double)Op / 10;
        }
        public static string Thickness()
        {
            return $"height: {HeaderHeight}px;";
        }
        public static string MinHeight()
        {
            return $"min-height: {HeaderHeight}px;";
        }
        public static string Width(int width)
        {
            return $"width: {width}px;";
        }
        public static string TextAlign()
        {
            string pad = "";
            // switch (TitleAlign)
            // {
            //     case "right":
            //         pad = $"padding-right:{HeaderHeight * 3}px;";
            //         break;
            //     case "left":
            //         pad = $"padding-left:{HeaderHeight}px;";
            //         break;
            // }
            return $"text-align: {TitleAlign};{pad}";
        }
        public static string Corners()
        {
            return $"border-radius: {Radius}px;";
        }
    }
}
