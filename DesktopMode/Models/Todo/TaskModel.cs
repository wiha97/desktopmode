﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models.Todo
{
    public class TaskModel
    {
        public int ID { get; set; }
        public string TempName { get; set; }
        public string Name { get; set; }
        public DateTime? TempDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }
        public bool IsSelected { get; set; }
    }
}
