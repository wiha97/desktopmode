﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models.Todo
{
    public class TodoManager
    {
        public List<TaskModel> Tasks = new List<TaskModel>();
        private int id = 0;
        public string TaskName { get; set; } = "";
        public string ButtonText = "";
        public DateTime? TaskDate;

        public void AddTask()
        {
            Tasks.Add(new TaskModel
            {
                ID = id++,
                Name = TaskName,
                Date = TaskDate,
                TempName = TaskName,
                TempDate = TaskDate
            });
        }

        public void DelTask()
        {
            Tasks.RemoveAll(t => t.IsSelected);
        }

        public void EditTask(TaskModel task)
        {
            task.Name = task.TempName;
            task.Date = task.TempDate;
            task.IsSelected = false;
        }

        public void Cancel(TaskModel task)
        {
            task.IsSelected = false;
        }

        public void SelAll()
        {
            bool select = true;
            if(Tasks.Where(t => t.IsSelected).Count() == Tasks.Count())
            {
                select = false;
            }
            foreach (TaskModel task in Tasks)
            {
                task.IsSelected = select;
            }
        }
    }
}
