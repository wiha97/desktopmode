﻿using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models
{
    public class DesktopManager
    {
        //TODO:
        //Replace 90% of this class with a proper manager

        //Bugs:
        //  MousePosition fails on Firefox

        public event PropertyChangedEventHandler PropertyChanged;
        public string[] WinTypes { get; set; } = { "About", "Help", "App", "Web", "Widget" };
        public List<PanelModel> Panels { get; set; }
        //public List<DesktopWindow> Windows { get; set; } = new List<DesktopWindow>();
        private DesktopWindow selWin;
        public LauncherModel Launcher { get; set; } = new LauncherModel();
        public Marker Mark;
        public bool RadMenu { get; set; }
        public string Blanket { get; set; } = $"width:0%;height:0%;";
        public int mouseX = 0;
        private int mouseY = 0;
        public string MousePos { get; set; } = "";
        private bool isDragging = false;
        private bool isScalingX = false;
        private bool isScalingY = false;
        bool expanded = false;
        public string LauncherHeight { get; set; } = "height:0px;";
        bool isMarking = false;
        private string _cursor = "";

        public DesktopManager()
        {   
            FillDocks();
            
            Panels = new()
            {
                new PanelModel()
                {
                    DockPos = Theme.DockPositions.Where(p => p.Name == "Bottom Center").FirstOrDefault().Value
                    // DockPos = "bottom:0;justify-content:center;"
                }
            };
            
            Welcome();
            // WM.GetApps();
            // WM.LaunchApp(WM.Windows.Where(w => w.Name == "settings").First());
        }

        async void Welcome()
        {
            await Task.Delay(500);
            WM.LaunchApp(WM.GetWindowByName("welcome"));
        }

        void FillDocks()
        {
            Theme.DockPositions = new()
            {
                new PanelDock()
                {
                    Name = "Top Left",
                    Value = "top:0;left:0;"
                },
                new PanelDock()
                {
                    Name = "Top Center",
                    Value = $"top:0;{Center()}"
                },
                new PanelDock()
                {
                    Name = "Top Right",
                    Value = "top:0;right:0;"
                },
                new PanelDock()
                {
                    Name = "Center Right",
                    Value = $"right:0;{Middle()}"
                },
                new PanelDock()
                {
                    Name = "Bottom Right",
                    Value = "bottom:0;right:0;"
                },
                new PanelDock()
                {
                    Name = "Bottom Center",
                    Value = $"bottom:0;{Center()}"
                },
                new PanelDock()
                {
                    Name = "Bottom Left",
                    Value = "bottom:0;left:0;"
                },
                new PanelDock()
                {
                    Name = "Center Left",
                    Value = $"left:0;{Middle()}"
                }
            };
        }

        private string Center()
        {
            return "margin-left:auto;margin-right:auto;left:0;right:0;";
        }

        private string Middle()
        {
            return "margin-top:auto;margin-bottom:auto;bottom:0;top:0;";
        }
        public void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
        }

        public void Marker()
        {
            if (isMarking)
            {
                if (mouseX < Mark.Xposition)
                    Mark.Xposition = mouseX;
                else
                    Mark.Width = mouseX - Mark.Xposition;
                if (mouseY < Mark.Yposition)
                    Mark.Yposition = mouseY;
                else
                    Mark.Height = mouseY - Mark.Yposition;
            }
        }
        private bool isOver = false;
        public void MouseOver()
        {
            isOver = true;
        }
        public void SetActive(DesktopWindow win)
        {
            win.IsActive = true;
            Launcher.Show = false;
            MouseOver();
        }
        public void DeActive(DesktopWindow win)
        {
            win.IsActive = false;
            MouseOut();
        }
        public void MouseOut()
        {
            isOver = false;
        }

        public void SetMarker()
        {
            if (!isDragging && !isScalingX && !isScalingY && !isOver)
            {
                ExpandBlanket();
                Mark = new Marker();
                isMarking = true;

                int x = mouseX, y = mouseY;
                if(x == 0 && y == 0)
                {
                    x = mouseXG;
                    y = mouseYG;
                }
                Mark.Xposition = mouseXG;
                Mark.Yposition = mouseYG;
            }
        }

        //public void StartApp(DesktopWindow win)
        //{
        //    //DesktopWindow win = null;
        //    //switch (app.ToLower())
        //    //{
        //    //    case "help": win = new HelpWindow(); break;
        //    //    case "settings": win = new SettingsWindow(); break;
        //    //    case "browser": win = new WebWindow(); break;
        //    //    case "about": win = new AboutWindow(); break;
        //    //    case "todo": win = new TodoWindow(); break;
        //    //    case "breakout": win = new BreakoutWindow(); break;
        //    //    case "memory": win = new MemoryWindow(); break;
        //    //    case "projects": win = new ProjectsWindow(); break;
        //    //    case "skills": win = new SkillsWindow(); break;
        //    //    default: win = new HelpWindow(); break;
        //    //}
        //    Windows.Add(win);
        //    PropChanged(win);
        //}

        //public void ExpandLauncher()
        //{
        //    if (expanded)
        //    {
        //        LauncherHeight = "height:0px;";
        //        expanded = false;
        //    }
        //    else
        //    {
        //        LauncherHeight = "height:25%;";
        //        expanded = true;
        //    }
        //}

        int xOffset = 0;
        int yOffset = 0;
        int oldPos = 0;
        int velocity = 0;
        public void MousePosition(MouseEventArgs e)
        {
            mouseX = (int)e.OffsetX;
            mouseY = (int)e.OffsetY;
            if (selWin != null)
            {
                if (xOffset == 0)
                    xOffset = mouseX - (int)selWin.XPosition;
                if (yOffset == 0)
                    yOffset = mouseY - (int)selWin.YPosition;
            }
            if (isDragging)
            {
                oldPos = (int)selWin.XPosition;
                selWin.IsActive = true;
                //WM.MoveWindow(selWin, mouseX, mouseY, xOffset, yOffset);
            }
            //MoveWindow(selWin);
            ScaleWindow();
            Marker();
        }
        int mouseXG = 0;
        int mouseYG = 0;
        public void GlobalPosition(MouseEventArgs e)
        {
            mouseXG = (int)e.ScreenX;
            mouseYG = (int)e.ScreenY;
        }
        public void ExpandBlanket()
        {
            Blanket = $"width:100%;height:100%;cursor:move;{_cursor}";
        }
        public void ClickHeader(DesktopWindow win)
        {
            ExpandBlanket();
            selWin = win;
            win.IsActive = true;
            isDragging = true;
            WM.PropChanged(selWin);
        }
        public void ClickSide(DesktopWindow win)
        {
            ExpandBlanket();
            selWin = win;
            isScalingX = true;
        }
        public void ClickBottom(DesktopWindow win)
        {
            ExpandBlanket();
            selWin = win;
            isScalingY = true;
        }
        public void ClickCorner(DesktopWindow win)
        {
            ExpandBlanket();
            selWin = win;
            isScalingY = true;
            isScalingX = true;
        }

        void MoveWindow(DesktopWindow win)
        {
            if (isDragging)
            {
                //win.YPosition = mouseY - 17;
                //win.XPosition = mouseX - win.Width / 2;
                win.YPosition = mouseY - yOffset;
                win.XPosition = mouseX - xOffset;
            }
        }
        public void ScaleWindow()
        {
            if (isScalingX && mouseX > selWin.XPosition)
            {
                selWin.Width = mouseX - (int)selWin.XPosition;
            }
            if (isScalingY && mouseY > selWin.YPosition)
            {
                selWin.Height = mouseY - (int)selWin.YPosition;
            }
        }

        public void Release()
        {
            if(selWin != null)
                velocity = (int)selWin.XPosition - oldPos;
            Console.WriteLine($"Velocity: {velocity}");
            Blanket = "width:0%;height:0%;";
            xOffset = 0;
            yOffset = 0;
            isDragging = false;
            isScalingX = false;
            isScalingY = false;
            isMarking = false;
            Mark = null;
        }

        public void ShadeWindow(DesktopWindow window)
        {
            if (window.IsShaded)
                window.IsShaded = false;
            else
                window.IsShaded = true;
        }

        public void SetCursor(string cursor)
        {
            _cursor = $"cursor:{cursor};";
            if (cursor == "")
                _cursor = "";
            Console.WriteLine(cursor);
        }

        //public void CloseWindow(DesktopWindow window)
        //{
        //    Windows.Remove(window);
        //    isOver = false;
        //}
    }
}
