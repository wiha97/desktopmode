﻿namespace DesktopMode.Models
{
    public class PanelModel
    {
        public int Width { get; set; } = 85;
        public int Height { get; set; } = 69;
        public bool Float { get; set; } = true;
        public bool Side { get; set; }
        public bool AutoHide { get; set; }
        public string DockPos { get; set; }
        // public PanelDock Pos { get; set; }
        public string Flex { get; set; } = "flex-direction: row;";

        private string width = "";
        private string height = "";

        public PanelModel()
        {
            // DockPos = Theme.DockPositions[Snap].Value;
            // DockPos = Positions[Snap];
            // Console.WriteLine($"Snap: {Snap}");
        }

        public string Dock()
        {
            switch (Theme.DockPositions.Where(d => d.Value == DockPos).FirstOrDefault().Name.ToLower())
            {
                case "center right":
                    Side = true;
                    break;
                case "center left":
                    Side = true;
                    break;
                case "bottom center":
                    Side = false;
                    break;
                case "top center":
                    Side = false;
                    break;
            }

            height = $"height:{Height}px;";
            width = $"width:{Width}%;";
            Flex = "flex-direction: row;";
            if (Side)
            {
                height = $"height:{Width}%;";
                width = $"width:{Height}px;";
                Flex = "flex-direction: column;";
            }

            string pos = $"position:absolute;{DockPos} {height} {width}";
            if (Float)
            {
                pos += $"padding:5px;";
            }

            return pos;
        }

        public string Position()
        {
            string size = $"height:100%;width:100%;";
            return $"position:relative; {size}";
        }
    }

    public class PanelDock
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}