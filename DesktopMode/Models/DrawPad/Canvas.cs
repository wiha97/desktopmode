﻿using System;
using System.Collections.Generic;

namespace DesktopMode.Models.DrawPad
{
    public class CanvasModel
    {
        public List<DrawCell> Cells { get; set; } = new List<DrawCell>();
        public int Width { get; set; }
        public int Height { get; set; }
        public string Unit { get; set; } = "px";
        public int Resolution { get; set; }
        public string Css()
        {
            return $"width:{Width}{Unit}; height:{Height}{Unit};";
        }
        public CanvasModel(int width, int height, string u, int res)
        {
            Width = width;
            Height = height;
            Unit = u;
            Resolution = res;
            GenerateGrid();
        }
        public void GenerateGrid()
        {
            Cells = new List<DrawCell>();

            //int res = 10;
            int max = (Width * Height) / (Resolution * Resolution);
            int left = 0;
            int top = 0;
            for (int i = 0; i < max; i++)
            {
                if(i % (Width / Resolution) == 0 && i != 0)
                {
                    left = 0;
                    top += Resolution;
                }
                Cells.Add(new DrawCell() { Color = "white", X = left, Y = top, Resolution = Resolution, Unit = Unit });
                left += Resolution;
            }
            Console.WriteLine(Css());
        }
    }
}
