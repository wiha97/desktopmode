﻿namespace DesktopMode.Models.DrawPad
{
    public class DrawCell
    {
        public int Resolution { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Unit { get; set; }
        public string Color { get; set; }
        public string Css()
        {
            return $"left:{X}{Unit}; top:{Y}{Unit}; width:{Resolution}{Unit}; height: {Resolution}{Unit}; background-color:{Color};";
        }
    }
}
