﻿namespace DesktopMode.Models.DrawPad
{
    public class BrushModel
    {
        public string Color { get; set; } = "black";
        public int Size { get; set; } = 1;

        public BrushModel(string color, int size)
        {
            Color = color;
            Size = size;
        }
    }
}
