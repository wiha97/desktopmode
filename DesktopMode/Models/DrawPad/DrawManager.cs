﻿using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DesktopMode.Models.DrawPad
{
    public class DrawManager
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string[] Colors { get; set; } = { "black", "blue", "green", "red", "white" };
        public CanvasModel Canvas { get; set; }
        public BrushModel Brush { get; set; }
        public int Resolution { get; set; } = 10;
        public int Width { get; set; } = 250;
        public int Height { get; set; } = 250;

        private bool draw;

        public DrawManager()
        {
            NewBrush("black", 1);
            SetCanvas();
        }

        public void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
        }

        public void NewBrush(string color, int size)
        {
            Brush = new BrushModel(color, size);
            PropChanged(Brush);
        }

        public void Toggle()
        {
            if (draw)
                draw = false;
            else
                draw = true;
        }

        int mouseX;
        int mouseY;
        public void MousePos(MouseEventArgs e)
        {
            mouseX = (int)e.OffsetX;
            mouseY = (int)e.OffsetY;
            Console.WriteLine(mouseX);
            if (draw)
            {
                Console.WriteLine($"draw: {draw}");
                MultiPaint();
            }
        }

        void MultiPaint()
        {
            List<DrawCell> cells = Canvas.Cells.Where(c => (c.X >= mouseX - Resolution) && (c.X <= mouseX + Resolution) && (c.Y >= mouseY - Resolution) && (c.Y <= mouseY + Resolution)).ToList();
            foreach (DrawCell cell in cells)
            {
                cell.Color = Brush.Color;
            }
            Console.WriteLine($"cells: {cells.Count}");
        }

        public void SetCanvas()
        {
            Canvas = new CanvasModel(Width, Height, "px", Resolution);
            PropChanged(Canvas);
        }
    }
}
