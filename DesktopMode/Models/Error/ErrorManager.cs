using System.Runtime.InteropServices;

namespace DesktopMode.Models.Error;

public class ErrorManager
{
    public static List<ErrorModel> Errors { get; set; } = new();

    public static void NewError(string msg, Exception? ex, int lvl)
    {
        Errors.Add(new ErrorModel()
        {
            Error = ex,
            Message = msg,
            Level = lvl
        });
    }
}