namespace DesktopMode.Models;

public class ErrorModel
{
    public string? Message { get; set; }
    public Exception? Error { get; set; }
    public int Level { get; set; }
}