using System.Numerics;

namespace DesktopMode.Models.RadialMenu;

public class RadBase
{
    public int Radius { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public Vector2 Pos { get; set; }

    public string CSS()
    {
        return $"left: {Pos.X}px; top: {Pos.Y}px;width: {Radius*2}px;";
    }
}