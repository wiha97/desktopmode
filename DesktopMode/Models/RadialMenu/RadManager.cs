using System.Numerics;
using Microsoft.AspNetCore.Components.Web;

namespace DesktopMode.Models.RadialMenu;

public class RadManager
{
    public List<RadItem> Rads { get; set; } = new();
    public RadBase Base { get; set; }
    public List<string> Types { get; set; } = new();

    private int mouseX = 0;
    private int mouseY = 0;

    private int Xp = 0;

    private int Yp = 0;
    int idx = 0;

    private int stepX = 0;
    private int stepY = 0;
    // public int Radius { get; set; } = 25;

    public RadManager()
    {
        Base = new RadBase()
        {
            Radius = 150,
            Pos = new Vector2(50,50)
            // X = 50,
            // Y = 50
        };
        Yp = Base.Radius / 2;
        Types = WM.DM.WinTypes.ToList();
        // GenRads();
    }

    public void PlaceRad()
    {
        // GenRads();
        // Rads.Add(new RadItem()
        // {
        //     X = mouseX - 20,
        //     Y = mouseY - 20
        // });
    }

    void GenRads()
    {
        double circ = Base.Radius * 3.14 * 2;
        int
            x = Base.X,
            y = Base.Y;
        // Console.WriteLine(circ);
        List<DesktopWindow> wins = WM.Windows.OrderBy(w => w.Name).ToList();

        stepX = (int)(circ / wins.Count);
        stepY = stepX;
        WM.window.SetStatus(stepX.ToString());
        WM.PropChanged(WM.window);
        foreach (DesktopWindow win in wins)
        {
            idx = wins.IndexOf(win);
            // FlipX(x);
            Rads.Add(new RadItem()
            {
                // X = (int)circ / (wins.IndexOf(win)+1),
                // Y = (int)circ / (wins.IndexOf(win)+1),
                // X = (wins.IndexOf(win))*x,
                X = Xpos(wins.IndexOf(win), x),
                // Y = Ypos(wins.IndexOf(win), y),
                // X = Xp++ * (int) (Base.Radius * 3.14 * 2) / WM.Windows.Count,
                // Y = Yp++ * (int) (Base.Radius * 3.14 * 2) / WM.Windows.Count,
                Y = 5,
                Win = win
            });
            x++;
            // FlipX(x);
        }
    }

    private int Xpos(int id, int bX)
    {
        // Xp *= (int) (Base.Radius * 3.14 * 2) / WM.Windows.Count;
        FlipX();
        if (!flipX)
        {
            Xp += stepX;
        }
        else
        {
            Xp -= stepX;
        }

        // return Xp * (int) (Base.Radius * 3.14 * 2) / WM.Windows.Count;
        return Xp;
    }

    private int Ypos(int id, int bY)
    {
        FlipY(Yp);
        if (!flipX)
        {
            Yp++;
        }
        else
        {
            Yp--;
        }

        return Yp * (int) (Base.Radius * 3.14 * 2) / WM.Windows.Count;
    }

    private bool flipX;

    private void FlipX()
    {
        if (idx > WM.Windows.Count / 2)
        {
            flipX = true;
        }
    }

    private bool flipY;

    private void FlipY(int bY)
    {
        if (bY > WM.Windows.Count / 2)
        {
            flipY = true;
        }
    }

    public void Mouse(MouseEventArgs e)
    {
        // WM.window.SetStatus($"{e.OffsetX}x{e.OffsetY}");
        mouseX = (int) e.OffsetX;
        mouseY = (int) e.OffsetY;
    }
}