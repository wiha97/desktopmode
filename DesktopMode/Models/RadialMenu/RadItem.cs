namespace DesktopMode.Models.RadialMenu;

public class RadItem
{
        public int? X { get; set; }
        public int? Y { get; set; }
        public DesktopWindow? Win { get; set; }

        public string CSS()
        {
                return $"left:{X}px; top: {Y}px;";
        }
}