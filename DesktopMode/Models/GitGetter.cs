﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DesktopMode.Models
{
    public class GitGetter
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public List<GitLabRelease> Releases { get; set; } = new List<GitLabRelease>();
        public string Repo { get; set; } = $@"https://gitlab.com/chupacode/greater-ark";
        public string Proxy { get; set; } = $@"https://thingproxy.freeboard.io/fetch/";

        public async void Getter()
        {
            //await Task.Run(() =>
            //{
            //    GetReleases();
            //});

            ////await Task.Delay(1000);
            //PropChanged(Releases);
            GetTask().Wait();
        }
        public void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
        }

        private Task GetTask()
        {
            return Task.Run(() => { GetReleases(); });
        }

        public async void GetReleases()
        {
            string json = "";
            try
            {
                string url = Repo + @"/-/releases.json";

                HttpClient http = new HttpClient();

                json = await http.GetStringAsync(Proxy + url);
                try
                {

                    //Deserializes into release objects
                    Releases = JsonSerializer.Deserialize<List<GitLabRelease>>(json);

                    //foreach(GitLabRelease rel in Releases)
                    Parallel.ForEach(Releases, rel =>
                    {
                        Console.WriteLine(rel.Name);
                        if (rel.Description.Contains(Environment.NewLine))
                        {
                            List<string> lines = rel.Description.Split(Environment.NewLine).ToList();
                            foreach (string line in lines)
                            {
                                if (!line.StartsWith('['))
                                {
                                    rel.Lines.Add(line);
                                }

                                //  Checks for embedded links
                                if (line.Contains("]("))
                                {
                                    string[] download = { };
                                    try
                                    {
                                        download = line.Split("](");
                                        string text = download[0];
                                        string link = download[1];

                                        int start = text.IndexOf('[') + 1;
                                        int endPar = link.IndexOf(')');

                                        //  Cleans up text and link parts to be usable for linking and easier to read
                                        text = text.Substring(start, text.Length - start);
                                        link = link.Substring(0, endPar);

                                        //  If the file was uploaded to this release, prefix the link with repo link
                                        if (link.StartsWith("/upload"))
                                            link = Repo + link;

                                        rel.Links.Add(new Download()
                                        {
                                            Text = text,
                                            Link = link
                                        });
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
    public class GitLabRelease
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("tag")]
        public string Tag { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("released_at")]
        public string Released { get; set; }

        public List<string> Lines { get; set; } = new List<string>();
        public List<Download> Links { get; set; } = new List<Download>();
    }

    public class Download
    {
        public string Link { get; set; }
        public string Text { get; set; }
    }

}
