namespace DesktopMode.Models.BlazeOut;

public class CellModel
{
    public int XPos { get; set; }
    public int YPos { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public string? Txt { get; set; }
    public int HP { get; set; }
    // public string[] Color = {"green", "cyan", "blue", "red"};
    public string Color { get; set; }

    public Powerup? Powerup { get; set; }

    public bool Left { get; set; } = false;
    public bool Top { get; set; } = false;
    public bool Right { get; set; } = false;
    public bool Bot { get; set; } = false;

    public bool Free { get; set; } = false;

    public string CSS()
    {
        if (Left || Top || Right || Bot)
        {
            Free = true;
        }
        return $"left: {XPos}px; top: {YPos}px; width: {Width}px; height: {Height}px; background-color: {Conf.Colors[HP]}; {Border()}";
    }

    string Border()
    {
        string left = "", top = "", right = "", bot = "";
        if (Left)
            left = "border-left:2px solid purple;";
        if (Top)
            top = "border-top:2px solid purple;";
        if (Right)
            right = "border-right:2px solid purple;";
        if (Bot)
            bot = "border-bottom:2px solid purple;";
        return $"{left} {top} {right} {bot}";
    }
    
    public void Drop()
    {
        YPos += Height;
    }

    public void Hit()
    {
        HP--;
        Color = Conf.Colors[HP];
    }
}