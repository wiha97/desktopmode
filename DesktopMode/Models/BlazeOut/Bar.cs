namespace DesktopMode.Models.BlazeOut;

public class BarModel
{
    public int XPos { get; set; }
    public int YPos { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public float Velocity { get; set; }

    public string CSS()
    {
        return $"left: {XPos}px; width: {Width}px; height: {Height}px; top: {YPos}px;";
    }
    
}