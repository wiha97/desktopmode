using System.ComponentModel;
using System.Net.Sockets;
using System.Numerics;
using Microsoft.AspNetCore.Components.Web;

namespace DesktopMode.Models.BlazeOut;

public class BlazeManager
{
    private int playWidth;
    private int playHeight;
    private int level;
    private int TickRate { get; set; } = 1;
    public int Score { get; set; }
    public bool IsRunning { get; set; }
    public bool GameOver { get; set; }
    private bool Drop { get; set; }
    private DesktopWindow win;
    public Projectile Ball { get; set; }
    private Controller controller = new();
    public BarModel Bar { get; set; }
    public List<Powerup> Powerups { get; set; }
    public List<CellModel> CellGrid { get; set; } = new List<CellModel>();

    public BlazeManager(DesktopWindow window)
    {
        win = window;
        GetPlaySpace();
        Ball = new Projectile();
        Bar = new BarModel();
    }

    public event PropertyChangedEventHandler PropertyChanged;

    void PropChanged(object obj)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(obj)));
    }

    public void MousePos(MouseEventArgs e)
    {
        int oldPos = Bar.XPos;
        Bar.XPos = (int) e.OffsetX - (Bar.Width / 2);
        Bar.Velocity = Bar.XPos - oldPos;
        if (Ball.YVel == 0)
        {
            Ball.XPos = (int) e.OffsetX;
            Ball.YPos = Bar.YPos - Ball.Radius;
        }
    }

    public void Start()
    {
        IsRunning = true;
        GameOver = false;
        Ball = new Projectile();
        Bar = new BarModel();
        // level = 10;
        CellGrid = new List<CellModel>();
        GenerateCells();

        Bar.Height = playHeight / 60;
        Bar.Width = playWidth / 5;
        Bar.YPos = playHeight - 200;

        MainLoop();
        // SecLoop();
        // Console.WriteLine("start");
        WM.Lock = true;
    }

    public void Pause()
    {
        IsRunning = !IsRunning;
        WM.Lock = !WM.Lock;
        MainLoop();
    }

    async void MainLoop()
    {
        while (IsRunning)
        {
            CellGrid.RemoveAll(c => c.HP < 1);
            GetPlaySpace();
            CheckFreeCells();
            // if (Ball.YVel == 0)
            // {
            //     Ball.XPos = Bar.XPos + (Bar.Width / 2);
            //     Ball.YPos = Bar.YPos - Ball.Radius;
            // }
            Ball.Move();
            if (Drop && Ball.YPos > playHeight - 300)
                CellDrop();

            if (Bar.Velocity > 0f)
                Bar.Velocity -= 0.1f;
            if (Bar.Velocity < 0f)
                Bar.Velocity += 0.1f;

            win.SetStatus(Bar.Velocity.ToString());

            BallCheck();
            WM.PropChanged(Ball);
            WM.PropChanged(CellGrid);
            await Task.Delay(TickRate);
        }
    }

    async void SecLoop()
    {
        while (IsRunning)
        {
            WM.PropChanged(win);
            await Task.Delay(100);
        }
    }

    public void Click()
    {
        Ball.Launch(-1.5f, Bar.Velocity);
    }

    void BallCheck()
    {
        if (Ball.YVel != 0)
        {
            if (Ball.YPos <= 0)
                Ball.BounceTop();

            // Vector4 ball = new Vector4();

            if (Ball.YPos + Ball.Radius >= Bar.YPos && Ball.YPos < Bar.YPos + Bar.Height &&
                PE.IsInside((int) Ball.XPos, Ball.Radius, Bar.XPos, Bar.Width)) // do proper collision check
            {
                Ball.BounceBar(Bar.Velocity);
            }

            if (Ball.YPos > playHeight - 50)
            {
                GameOver = true;
                IsRunning = false;
            }

            if (Ball.XPos <= 0 ||
                Ball.XPos + Ball.Radius >= playWidth - 1)
                Ball.BounceSide();

            if (Ball.YPos < playHeight - 250)
                CheckGrid();
        }
    }

    void CheckGrid()
    {
        foreach (CellModel cell in CellGrid.Where(c => c.YPos + c.Height >= Ball.YPos + Ball.Radius &&
                                                       c.YPos <= Ball.YPos + (Ball.Radius * 2) && c.Free))
        {
            if (PE.IsInside((int) Ball.XPos, Ball.Radius, cell.XPos, cell.Width) &&
                PE.IsInside((int) Ball.YPos, Ball.Radius, cell.YPos, cell.Height))
            {
                cell.Hit();
            }
        }
    }

    void GenerateCells()
    {
        Console.Clear();
        int y = 0;
        for (int i = 10; i >= 0; i--)
        {
            AddRow(i);
            Console.WriteLine(playWidth);
            // CellGrid.Concat(Row(x, y));
        }
    }

    void CellDrop()
    {
        foreach (CellModel cell in CellGrid)
        {
            cell.YPos += cell.Height;
            // cell.Hit();
            // if (cell.YPos >= playHeight - 200)
            //     CellGrid.Remove(cell);
        }

        CellGrid.RemoveAll(c => c.YPos > playHeight - 500);
        win.SetStatus(level.ToString());
    }

    void AddRow(int top)
    {
        int width = playWidth / 8;
        int height = playHeight / 25;
        int x = 0;

        while (true)
        {
            if (x + width > playWidth)
                break;
            int chance = new Random().Next(0, 100);
            int hp = 1;

            if (level <= 10)
            {
                hp = new Random().Next(1, 3);
            }
            else
            {
                hp = new Random().Next(1, 5);
            }

            // else
            // {
            //     hp = 1;
            // }
            int rnd = new Random().Next(1, 10);

            if (rnd < 3)
                hp = 0;

            if (hp > 0)
            {
                CellGrid.Add(new CellModel()
                {
                    Width = width,
                    Height = height,
                    XPos = x,
                    YPos = top * height,
                    HP = 1
                    // Txt = rnd.ToString()
                });
            }

            x += width;
        }
        CheckFreeCells();

        Console.WriteLine(level);
        level++;
    }

    void CheckFreeCells()
    {
        foreach (CellModel cell in CellGrid)
        {
            // bool left = false, right = false, top = false, bot = false;
            // if (CellGrid.Where(c => c.XPos + c.Width == cell.XPos).FirstOrDefault() != null &&
            //     CellGrid.Where(c => c.YPos + c.Height == cell.YPos).FirstOrDefault() != null)

            List<CellModel> xCells = CellGrid.Where(c => c.YPos == cell.YPos).OrderBy(c => c.XPos).ToList();
            int xId = xCells.IndexOf(cell);
            List<CellModel> yCells = CellGrid.Where(c => c.XPos == cell.XPos).OrderBy(c => c.YPos).ToList();
            int yId = yCells.IndexOf(cell);
            cell.Txt = $"{yId}";

            // if (xId == 0)
            //     xId = 1;
            // if (xId == xCells.Count -1)
            //     xId = xCells.Count -2;
            // if (yId == 0)
            //     yId = 1;
            // if (yId == yCells.Count - 1)
            //     yId = yCells.Count - 2;

            CellModel leftCell;
            CellModel topCell;
            CellModel rightCell;
            CellModel botCell;
            

            // try
            // {
            if (xId != 0)
            {
                leftCell = xCells[xId - 1];
                if (leftCell.XPos + leftCell.Width != cell.XPos)
                    cell.Left = true;
            }
            else if (cell.XPos != 0)
                cell.Left = true;
            if (xId != xCells.Count - 1)
            {
                rightCell = xCells[xId + 1];
                if (rightCell.XPos != cell.XPos + cell.Width)
                    cell.Right = true;
            }
            else if (cell.XPos != xCells.Count - 1)
                cell.Right = true;

            if (yId != 0)
            {
                topCell = yCells[yId - 1];
                if (topCell.YPos + topCell.Height != cell.YPos)
                    cell.Top = true;
            }
            else if (cell.YPos != 0) // This is bugged
            {
                cell.Txt = "top";
                cell.Top = true;
            }

            // cell.Top = true;
            if (yId != yCells.Count - 1)
            {
                botCell = yCells[yId + 1];
                if (botCell.YPos != cell.YPos + cell.Height)
                    cell.Bot = true;
            }
            else
                cell.Bot = true;

            
        }
    }

    private bool CheckSide(int i)
    {
         
        return false;
    }

    //      Can change dynamically
    void GetPlaySpace()
    {
        playWidth = win.Width;
        playHeight = win.Height;
    }
}