namespace DesktopMode.Models.BlazeOut;

public class Collider
{
    public static bool InRange(float value, float min, float max)
    {
        if (value >= Math.Min(min, max) && value <= Math.Min(min, max))
        {
            return true;
        }
        return false;
    }
}