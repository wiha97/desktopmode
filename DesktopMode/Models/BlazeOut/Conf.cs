namespace DesktopMode.Models.BlazeOut;

public class Conf
{
    public static string[] Colors = {"white", "black", "cyan", "blue", "red", "purple"};
    public static float MaxVel { get; set; } = 8f;
}