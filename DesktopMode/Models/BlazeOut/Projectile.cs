namespace DesktopMode.Models.BlazeOut;

public class Projectile
{
    public float XPos { get; set; }
    public float YPos { get; set; }
    public int Radius { get; set; } = 10;
    public float XVel { get; set; }
    public float YVel { get; set; }
    public Powerup? Power { get; set; }

    public string CSS()
    {
        return $"left: {(int) XPos}px; top: {(int) YPos}px; width: {Radius}px;";
    }

    public void Launch(float yVel, float xVel)
    {
        XVel = xVel;
        YVel = yVel;
        if (XVel < -Conf.MaxVel)
            XVel = -Conf.MaxVel;
        if (XVel > Conf.MaxVel)
            XVel = Conf.MaxVel;
    }

    public void Move()
    {
        XPos += XVel;
        YPos += YVel;
    }

    public void BounceTop()
    {
        YVel = YVel - (YVel * 2);
    }

    public void BounceSide()
    {
        XVel = XVel - (XVel * 2);
    }

    public void BounceBar(float velocity)
    {
        if (velocity - XVel > -10 && velocity - XVel < 10)
        {
            XVel += velocity;
            // if (XVel < -10)
            //     XVel = -10;
            // if (XVel > 10)
            //     XVel = 10;
        }
        if (XVel < -Conf.MaxVel)
            XVel = -Conf.MaxVel;
        if (XVel > Conf.MaxVel)
            XVel = Conf.MaxVel;
        BounceTop();
    }
}