using System.Numerics;

namespace DesktopMode.Models.BlazeOut;

public class SmartGrid
{
    public List<GridCell> Cells { get; set; }

}

public class GridCell
{
    public int X { get; set; }
    public int Y { get; set; }
}