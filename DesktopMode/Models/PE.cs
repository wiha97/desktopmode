using System.Security.Cryptography.X509Certificates;

namespace DesktopMode.Models;

public class PE
{
    public static bool IsColliding(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2)
    {
        // Top
        if (IsTouching(y1, h1, y2) && (IsInside(x1, w2, x2, w2)))
        {
            return true;
        }
        // Bot
        if (IsTouching(y1, h1, y2+h2) && IsInside(x1, w2, x2, w2))
        {
            return true;
        }
        // Left
        if (IsTouching(x1, w1, x2) && IsInside(y1, h2, y2, h2))
        {
            return true;
        }
        // Right
        if (IsTouching(x1, w1, x2+w2) && IsInside(y1, h2, y2, h2))
        {
            return true;
        }
        // if (IsWithin(x1, y1, w1, h1, x2, y2, w2, h2))
        // {
        //     return true;
        // }
        return false;
    }

    public static bool IsWithin(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2)
    {
        if (IsInside(x1, w1, x2, w2) &&
            IsInside(y1, h1, y2, h2))
        {
            return true;
        }
        return false;
    }
    
    public static bool IsTouching(int x1, int w1, int x2)
    {
        if (x1 <= x2 && x1+w1 >= x2)
        {
            return true;
        }
        return false;
    }
    public static bool IsInside(int x1, int w1, int x2, int w2)
    {
        if (x1 > x2 && x1+w1 < x2 + w2)
        {
            return true;
        }
        return false;
    }
}