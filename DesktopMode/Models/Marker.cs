﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesktopMode.Models
{
    public class Marker
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int Xposition { get; set; }
        public int Yposition { get; set; }

        public string Position()
        {
            return $"left:{Xposition}px; top:{Yposition}px;";
        }
        public string Scale()
        {
            return $"width:{Width}px; height:{Height}px;";
        }
    }
}
