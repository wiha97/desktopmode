﻿using System.Reflection;
using System.ComponentModel;
using Microsoft.AspNetCore.Components.Web;

namespace DesktopMode.Models
{
    //  Window manager, manages everything window
    public class WM
    {
        public static event PropertyChangedEventHandler PropertyChanged;
        public static int Wobblyness { get; set; } = 10;
        public static bool AutoHide {get; set; }
        public static void PropChanged(object obj)
        {
            PropertyChanged?.Invoke(DM, new PropertyChangedEventArgs(nameof(obj)));
        }
        public static void MousePosition(MouseEventArgs mouse)
        {
            if (!Lock)
            {
                mouseX = (int)mouse.PageX;
                mouseY = (int)mouse.PageY;
                if (window != null)
                {
                    oldX = window.XPosition;
                    oldY = window.YPosition;
                    if (window.IsDragging)
                        MoveWindow();
                    ScaleWindow();
                }
            }
        }

        public static DesktopManager DM { get; set; }
        public static DesktopWindow window { get; set; }
        public static List<DesktopWindow> Windows { get; set; } = new();
        private static List<DesktopWindow> activeWindows = new();
        public static List<DesktopWindow> ActiveWindows()
        {
            return activeWindows.OrderBy(w => w.Level).ToList();
        }
        public static List<Type> Apps { get; set; } = new List<Type>();
        public static List<Type> Classes { get; set; } = new List<Type>();
        public static List<Component> Components { get; set; } = new List<Component>();

        public static int minHeight = Theme.HeaderHeight;
        public static int minWidth = 200;

        static int oldX = 0;
        static int oldY = 0;

        public static int mouseX { get; set; } = 0;
        public static int mouseY { get; set; } = 0;

        static int offsetX = 0;
        static int offsetY = 0;

        private static int velocityX = 0;
        private static int velocityY = 0;

        public static bool scalingXL { get; set; }
        public static bool scalingXR { get; set; }
        public static bool scalingYT { get; set; }
        public static bool scalingYB { get; set; }
        public static bool Lock { get; set; } = false;
        private static bool initGrab;

        public static void ScaleWindow()
        {
            if (scalingXL && mouseX < window.XPosition + window.Width - minWidth)
            {
                window.XPosition = mouseX;
                window.Width -= window.XPosition - oldX;
            }
            if (scalingXR && mouseX > window.XPosition + minWidth)
            {
                window.Width = mouseX - window.XPosition;
            }
            if (scalingYT && mouseY < window.YPosition + window.Height - minHeight)
            {
                window.YPosition = mouseY;
                window.Height -= window.YPosition - oldY;
            }
            if (scalingYB && mouseY > window.YPosition + minHeight)
            {
                window.Height = mouseY - window.YPosition;
            }
        }

        public static void SelectWindow(DesktopWindow win)
        {
            win.IsActive = true;
            window = win;
            try
            {

                DM.Launcher.Show = false;
            }
            catch
            {
                
            }
        }
        public static void DeselectWindow()
        {
            if (window != null)
            {
                if(window.IsActive)
                    window.IsActive = false;
            }
        }

        public static void DragWindow()
        {
            if (window != null)
            {
                // Grabs the center of the titlebar if the window has just spawned (center of screen with left:0; value)
                // or if maxed (left:0; and width:100vw; value)
                // Will probably replace with a method that checks the position in the titlebar itself as percentage then does some math
                // but this works for now
                if (window.Spawn || window.IsMaxed)
                    initGrab = true;
                DM.ExpandBlanket();
                window.IsDragging = true;
                    offsetX = mouseX - window.XPosition;
                    offsetY = mouseY - window.YPosition;

                if (window.IsMaxed)
                {
                    window.IsMaxed = false;
                    // offsetX = mouseX - window.XPosition + (window.Width / 2);
                    offsetX = mouseX;
                    offsetY = mouseY;
                    window.SetStatus(offsetY.ToString());
                }
                MoveWindow();
            }
        }

        public static void Release()
        {
            if (window != null)
            {
                window.IsDragging = false;
                window.IsActive = false;
                DM.Blanket = "width:0%;height:0%;";
                DM.SetCursor("");

                scalingXL = false;
                scalingXR = false;
                scalingYB = false;
                scalingYT = false;
                window.Settle(velocityX, velocityY);
            }
            offsetX = 0;
            offsetY = 0;
            if (initGrab)
                initGrab = false;
        }

        public static Type? GetApp(string appName)
        {
            return Apps.FirstOrDefault(a => a.Name.ToLower() == appName.ToLower());
        }

        public static DesktopWindow? GetWindowByName(string name)
        {
            return Windows.FirstOrDefault(w => w.Name == name.ToLower());
        }

        public static void GetApps()
        {
            Apps = Assembly.GetExecutingAssembly().GetTypes().Where(a => a.FullName.Contains("Components.Apps") && !a.FullName.Contains("+<")).ToList();
            Classes = Assembly.GetExecutingAssembly().GetTypes().Where(c => c.FullName.Contains("Models") && !c.FullName.Contains("+<") && c.Name.Contains("Window")).ToList();
            foreach (Type c in Classes)
            {
                try
                {
                    DesktopWindow win = (DesktopWindow)Activator.CreateInstance(c);
                    if (win.Name != null)
                        Windows.Add(win);
                }
                catch
                {
                    Console.WriteLine($"Failed to add {c.Name}");
                }
            }
        }

        public static void LaunchApp(DesktopWindow app)
        {
            try
            {
                int xPos = app.XPosition;
                int yPos = app.YPosition;
                activeWindows.Add(new DesktopWindow()
                {
                    Name = app.Name,
                    Height = app.Height,
                    YeetAble = app.YeetAble,
                    Width = app.Width,
                    Icon = app.Icon,
                    Parameters = app.Parameters,
                    Type = app.Type,
                    XPosition = xPos,
                    YPosition = yPos,
                    Error = app.Error,
                });
                PropChanged(app);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}");
            }
        }
        public static void MoveWindow()
        {
            int oldPosX = window.XPosition;
            int oldPosY = window.YPosition;
            if(!initGrab)
            {
                window.YPosition = mouseY - offsetY;
                window.XPosition = mouseX - offsetX;
            }
            else
            {
                window.XPosition = mouseX - (window.Width / 2);
                window.YPosition = mouseY;
            }

            velocityX = window.XPosition - oldPosX;
            velocityY = window.YPosition - oldPosY;
            

            if(Theme.Wobble)
                window.Wobble = velocityX / 1.5 * Theme.Skew;

            if (window.IsDragging)
                window.Spawn = false;
        }

        public static void ShadeWindow(DesktopWindow window)
        {
            window.IsShaded = !window.IsShaded;
        }

        public static void CloseWindow(DesktopWindow window)
        {
            activeWindows.Remove(window);
            PropChanged(window);
        }
    }
}
