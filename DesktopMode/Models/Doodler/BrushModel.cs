﻿using System.Drawing;

namespace DesktopMode.Models.Doodler
{
    public class BrushModel
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int XPos { get; set; }
        public int YPos { get; set; }
        public string Color { get; set; }
        public string CSS()
        {
            return $"width: {Width}px; height: {Height}px; background-color: {Color}; left: {XPos}px; top: {YPos};";
        }

        //public BrushModel(int width, int height, string color, int xPos, int yPos)
        //{
        //    Width = width;
        //    Height = height;
        //    XPos = xPos;
        //    YPos = yPos;
        //    Color = color;
        //    CSS = $"width: {width}px; height: {height}px; background-color: {color}; left: {xPos}px; top: {yPos};";
        //}

        public void Position(int xPos, int yPos)
        {
            XPos = xPos;
            YPos = yPos;
        }
    }
}
