﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using System.Xml.Schema;
using Microsoft.AspNetCore.Components;

namespace DesktopMode.Models
{
    public class DesktopWindow
    {
        public string Name { get; set; }
        public int XPosition { get; set; } = 0;
        public int YPosition { get; set; } = 0;
        public Vector2 Pos { get; set; } = Vector2.Zero;
        public Vector4 Vec { get; set; } = Vector4.Zero;
        public int Width { get; set; } = 200;
        public string CurWidth { get; set; }
        public int Height { get; set; } = 200;
        public string? Icon { get; set; } = "assets/help.png";
        public string? Error { get; set; }
        public bool IsShaded { get; set; }
        public bool IsActive { get; set; }
        public bool IsDragging { get; set; }
        public bool IsMaxed { get; set; }
        public bool IsMinimized { get; set; }
        public bool Static { get; set; }
        public bool Spawn { get; set; } = true;
        public bool Parachute { get; set; }
        public bool MaxQ { get; set; }
        public bool YeetAble { get; set; }
        public string? Status { get; set; }
        public int Level { get; set; } = 1;
        public double Wobble { get; set; }
        private double tW;
        public Type? App { get; set; }
        public Dictionary<string, object> Parameters { get; set; } = new Dictionary<string, object>();
        public WinType Type { get; set; }

        public enum WinType
        {
            Help,
            About,
            Web,
            App,
            Popup,
            Widget
        }

        public DesktopWindow()
        {
            // Console.WriteLine($"Name: {Type.ToString()}");
            // if (Type.ToString() == "Help")
            //     YeetAble = true;
            if (!YeetAble)
            {
                YeetAble = Theme.YeetMode;
                // Settle(0, 0);
            }
        }

        protected void SetParam()
        {
            Parameters = new Dictionary<string, object>()
            {
                ["win"] = this
            };
        }

        public void SetStatus(string? status)
        {
            if (status != null)
                Status = $"- {status}";
            else
                Status = null;
        }

        public void Maximize()
        {
            IsMaxed = !IsMaxed;
            WM.AutoHide = !WM.AutoHide;
            //if (IsMaxed)
            //    IsMaxed = false;
            //else
            //    IsMaxed = true;
        }

        public void Minimize()
        {
            IsMinimized = !IsMinimized;
        }

        public void Shade()
        {
            IsShaded = !IsShaded;
        }

        public string InitPosition()
        {
            return $"margin:auto;";
        }

        public string Position()
        {
            if (Wobble > WM.Wobblyness)
                Wobble = WM.Wobblyness;
            if (Wobble < -WM.Wobblyness)
                Wobble = -WM.Wobblyness;

            if (Wobble > tW)
                tW = Wobble;

            // if (IsDragging)
            //     Spawn = false;
            if (Spawn)
            {
                if (Theme.YeetMode)
                    YeetAble = true;
                XPosition = (Theme.XResolution / 2) - (Width / 2);
                if (YeetAble)
                    YPosition = -Height * 2;
                else
                    YPosition = (Theme.YResolution / 2) - Height / 2;
                Spawn = false;
                Settle(0, 0);
            }
            // return
            //     "left:0;right:0;margin-left:auto;margin-right:auto;top:0;bottom:0;margin-top:auto;margin-bottom:auto;";

            if (IsMaxed)
                return $"left:0px;top:0px;";
            else
                return $"left:{XPosition}px; top: {YPosition}px; transform:skewX({-Wobble}deg);";
        }

        private float x = 0;
        private float y = 0;
        private PanelModel? panel;

        public async void Settle(int xvel, int yvel)
        {
            int delay = 1;
            // yvel /= 2;
            xvel /= 2;
            panel = WM.DM.Panels.FirstOrDefault(p => p.DockPos.ToLower().Contains("bottom"));
            int[] swayDir = { -3, 3 };
            int i = new Random().Next(0, 1);
            ;
            while (true)
            {
                if (YeetAble)
                {
                    delay = 10;
                    if (YPosition + Height < 0)
                        MaxQ = true;
                    //  Tried moving to method, broke it, so here it lays
                    float gravity = 0.6f;
                    int speed = 25;

                    if (x > speed)
                        x = speed;
                    if (x < -speed)
                        x = -speed;
                    if (y > speed * 2)
                        y = speed * 2;
                    if (y < -speed * 2)
                        y = -speed * 2;

                    float friction = 1;
                    if (Parachute)
                    {
                        friction = 0.15f;
                        // gravity = 0.1f;
                        y = 2;
                    }

                    float drag = 0.33f;
                    if (x > 0)
                        drag = -0.33f;
                    x += (xvel + drag) * friction;
                    if (XPosition < 0 || XPosition + Width > Theme.XResolution)
                        x = 0;

                    y += (yvel + 0.1f) * gravity + 0.33f;
                    int offset = 0;
                    if (panel != null)
                        offset = panel.Height;
                    if (YPosition + Height > Theme.YResolution - offset)
                    {
                        y = 0;
                        YPosition = Theme.YResolution - (offset + Height);
                    }

                    if (xvel > 0)
                        xvel--;
                    else if (xvel < 0)
                        xvel++;
                    if (yvel > 0)
                        yvel--;
                    else if (yvel < 0)
                        yvel++;

                    XPosition += (int)x;
                    YPosition += (int)y;

                    if (MaxQ && YPosition > Theme.YResolution / 3)
                    {
                        Parachute = true;
                        if (x < 0.1 && x > -0.1)
                        {
                            x = swayDir[i++];
                            if (i > 1)
                                i = 0;
                        }
                    }


                    if (Theme.Wobble)
                        Wobble = x / 1.5 * Theme.Skew;
                }

                if (Wobble > 0.3)
                {
                    Wobble -= 0.5;
                }
                else if (Wobble < -0.3)
                {
                    Wobble += 0.5;
                }
                else
                {
                    Wobble = 0;
                    // loop = false;
                }

                // y = Yeet(xvelocity, yvel);

                WM.PropChanged(this);
                Position();
                await Task.Delay(delay);
                if ((YeetAble && y == 0) || (!YeetAble && Wobble == 0))
                {
                    Wobble = 0;
                    break;
                }
            }

            MaxQ = false;
            Parachute = false;

            SetStatus(null);

            WM.PropChanged(this);
            Position();
        }

        bool Yeet(int xvelocity, int yvel)
        {
            xvelocity /= 2;
            yvel /= 2;
            Theme.YResolution = 1080;
            Theme.XResolution = 1920;
            float gravity = 0.6f;
            int speed = 25;

            if (x > speed)
                x = speed;
            if (x < -speed)
                x = -speed;
            if (y > speed)
                y = speed;
            if (y < -speed)
                y = -speed;

            float i = 0.33f;
            if (x > 0)
                i = -0.33f;
            x += (xvelocity + i) * 1f;
            if (XPosition < 0 || XPosition + Width > Theme.XResolution)
                x = 0;

            y += (yvel + 0.1f) * gravity + 0.33f;
            int offset = 0;
            if (panel != null)
                offset = panel.Height;
            if (YPosition + Height > Theme.YResolution - offset)
            {
                y = 0;
                YPosition = Theme.YResolution - (offset + Height);
            }

            if (xvelocity > 0)
                xvelocity--;
            else if (xvelocity < 0)
                xvelocity++;
            if (yvel > 0)
                yvel--;
            else if (yvel < 0)
                yvel++;

            XPosition += (int)x;
            YPosition += (int)y;


            Wobble = x / 1.5 * Theme.Skew;

            // if (values.Length > 2 && Math.Abs(x - values[values.Length - 2]) < 0.04)
            // if (XPosition == oldX)
            //     x = 0;
            // if(YPosition )
            //     y = 0;

            // if (x < 0.03 && x > -0.03)
            //     x = 0;

            SetStatus($"{x} | {yvel}");
            // Yeet(xvelocity, yvel);
            return true;
        }

        //  TODO: Clean this mess
        public string Scale()
        {
            string width = $"width:{Width}px;";
            string height = $"height:{Height}px;";
            int padding = 0;
            string overflow = "hidden";
            string css = "";
            if (IsMaxed)
            {
                width = $"width:100%;";
                height = $"height:100%;";
                //height = $"height:{Theme.YResolution - Theme.HeaderHeight - 2}px;";
            }

            if (IsShaded)
            {
                height = $"height:{Theme.HeaderHeight}px;";
                padding = 0;
                overflow = "hidden";
                css = $"{height} overflow-y:{overflow}; padding-top: {padding}px;";
                if (Width != 0)
                    css += $"{width}";
            }
            //else if (IsMaxed)
            //    css = $"width: 100%; height:100%;top:0px;left:0px;z-index:-1;";
            else if (Width != 0 && Height != 0)
                css = $"{width} {height}  padding-top: {padding}px;";
            else
                css = $"overflow-y:{overflow}; padding-top: {padding}px;";

            CurWidth = width;
            return css;
        }

        public void Raise()
        {
            if (Level == 2)
                Level--;
            else if (Level == 1)
                Level++;
            SetStatus(Level.ToString());
        }

        //public DesktopWindow()
        //{
        //    XPosition = new Random().Next(0, Theme.XResolution);
        //    YPosition = new Random().Next(0, Theme.YResolution);
        //}
    }

    public class WebBrowserWindow : DesktopWindow
    {
        public WebBrowserWindow()
        {
            Name = "webbrowser";
            Width = 1280;
            Height = 720;
            Type = WinType.Web;
            Icon = $@"assets/browser.png";
            Error = "Fixing bug that blew the site up (literally)";
        }
    }

    public class HelpWindow : DesktopWindow
    {
        public HelpWindow()
        {
            Name = "help";
            YeetAble = true;
            XPosition = 0;
            YPosition = 0;
            Width = 880;
            Height = 450;
            Type = WinType.Help;
            Icon = $"assets/help.png";
        }
    }

    public class SettingsWindow : DesktopWindow
    {
        public SettingsWindow()
        {
            Name = "settings";
            XPosition = 0;
            YPosition = 0;
            Width = 670;
            Height = 570;
            Type = WinType.Help;
            Icon = $"assets/settings.png";
            Parameters = new Dictionary<string, object>
            {
                ["win"] = this
            };
        }
    }

    public class MemoryWindow : DesktopWindow
    {
        public MemoryWindow()
        {
            Name = "memory";
            Width = 380;
            Height = 740;
            Type = WinType.App;
            Icon = $"assets/memory.png";
        }
    }

    public class DrawWindow : DesktopWindow
    {
        public DrawWindow()
        {
            Name = "draw";
            Width = 600;
            Height = 740;
            Type = WinType.App;
            Icon = $"assets/git.png";
        }
    }

    public class CVWindow : DesktopWindow
    {
        public CVWindow()
        {
            Name = "CV";
            Width = 445;
            Height = 700;
            Icon = $"assets/aboutme.png";
            Type = WinType.About;
            // Parameters = new Dictionary<string, object>
            // {
            //     ["win"] = this
            // };
        }
    }

    public class PinpadWindow : DesktopWindow
    {
        public PinpadWindow()
        {
            Name = "pinpad";
            Width = 270;
            Height = 250;
            Icon = $"assets/pinpad.png";
            Type = WinType.Widget;
        }
    }

    public class RollWindow : DesktopWindow
    {
        public RollWindow()
        {
            Name = "roll";
            Width = 300;
            Height = 500;
            Icon = $"assets/help.png";
            Type = WinType.Popup;
        }
    }
}